package dev.jbriggs.aoc2021.day8;

import static dev.jbriggs.aoc2021.day8.ConnectionSolver.getNumberFromCombinationMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 8 tests")
class Day8Test {
  Day8 day;

  @BeforeEach
  void beforeEach() {
    day = new Day8(new PuzzleInputParser());
  }

  @Nested
  @DisplayName("Part one tests")
  class PartOneTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 26 when test data entered")
    void shouldReturn5934WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day8/input.txt");
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("26"));
    }

  }

  @Nested
  @DisplayName("Part two tests")
  class PartTwoTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 8394 when one row of test data entered")
    void shouldReturn8394WhenTestDataEntered() {
      // Given
      String[] input =
          new String[] {
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"
          };
      Map<String, Integer> combinations = new ConnectionSolver().findConnectionCombinations(input[0]);
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(getNumberFromCombinationMap(combinations, "fdgacbe"), is(8));
      assertThat(getNumberFromCombinationMap(combinations, "cefdb"), is(3));
      assertThat(getNumberFromCombinationMap(combinations, "cefbgd"), is(9));
      assertThat(getNumberFromCombinationMap(combinations, "gcbe"), is(4));
      assertThat(result, is("8394"));
    }

    @Test
    @DisplayName("Should return 61229 when test data entered")
    void shouldReturn61229WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day8/input.txt");
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("61229"));
    }

  }


}