package dev.jbriggs.aoc2021.day8;

import static dev.jbriggs.aoc2021.day8.ConnectionSolver.getNumberFromCombinationMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Map;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 8 - Connection solver tests")
class ConnectionSolverTest {

  @Nested
  @DisplayName("Find number times 1 shows in 1 character combinations")
  class FindNumberTimes1ShowsInFourDigitCombinations{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfOnes(new String[0]);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 when no 2 character combinations found")
    void shouldReturn0WhenNo2CharacterCombinationsFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfOnes(new String[]{"abc abc abc abc"});
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 1 when 2 character combination found")
    void shouldReturn1When2CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfOnes(new String[]{"ab abc abc abc"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 2 character combination found")
    void shouldReturn2When2CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfOnes(new String[]{"ab bc abc abc"});
      // Then
      assertThat(result, is(2L));
    }

    @Test
    @DisplayName("Should return 1 when 2 character combination found in 2 rows")
    void shouldReturn1When2CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfOnes(new String[]{"ab abc abc abc", "abc abc abc abc"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 2 character combination found in 2 rows")
    void shouldReturn2When2CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfOnes(new String[]{"ab abc abc abc", "abc ab cbe e"});
      // Then
      assertThat(result, is(2L));
    }
  }

  @Nested
  @DisplayName("Find number times 4 shows in 4 character combinations")
  class FindNumberTimes4ShowsInFourDigitCombinations{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfFours(new String[0]);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 when no 4 character combinations found")
    void shouldReturn0WhenNo2CharacterCombinationsFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfFours(new String[]{"abc abc abc abc"});
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 1 when 4 character combination found")
    void shouldReturn1When2CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfFours(new String[]{"ab abce abc abc"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 4 character combination found")
    void shouldReturn2When2CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfFours(new String[]{"ab bcde abce abc"});
      // Then
      assertThat(result, is(2L));
    }

    @Test
    @DisplayName("Should return 1 when 4 character combination found in 2 rows")
    void shouldReturn1When2CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfFours(new String[]{"ab abce abc abc", "abc abc abc abc"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 4 character combination found in 2 rows")
    void shouldReturn2When2CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfFours(new String[]{"ab abc abce abc", "abce ab cbe e"});
      // Then
      assertThat(result, is(2L));
    }
  }

  @Nested
  @DisplayName("Find number times 7 shows in 3 character combinations")
  class FindNumberTimes7ShowsInFourDigitCombinations{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[0]);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 when no 3 character combinations found")
    void shouldReturn0WhenNo3CharacterCombinationsFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"abce abce abce abce"});
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 1 when 3 character combination found")
    void shouldReturn1When3CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"ab abc ab ab"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 3 character combination found")
    void shouldReturn2When3CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"ab abc ade a"});
      // Then
      assertThat(result, is(2L));
    }

    @Test
    @DisplayName("Should return 1 when 3 character combination found in 2 rows")
    void shouldReturn1When3CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"ab a abc a", "a a c d"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 3 character combination found in 2 rows")
    void shouldReturn2When3CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"ab abc e d", "a ab cbe e"});
      // Then
      assertThat(result, is(2L));
    }
  }

  @Nested
  @DisplayName("Find number times 8 shows in 7 character combinations")
  class FindNumberTimes8ShowsInFourDigitCombinations{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfEights(new String[0]);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 when no 7 character combinations found")
    void shouldReturn0WhenNo7CharacterCombinationsFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"abce abce abce abce"});
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 1 when 3 character combination found")
    void shouldReturn1When3CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"ab abcefge abe ab"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 3 character combination found")
    void shouldReturn2When3CharacterCombinationFound(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"abe abcdefg abcdefg ace"});
      // Then
      assertThat(result, is(2L));
    }

    @Test
    @DisplayName("Should return 1 when 7 character combination found in 2 rows")
    void shouldReturn1When7CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"abe a abcdefg a", "a a c d"});
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when 7 character combination found in 2 rows")
    void shouldReturn2When7CharacterCombinationFoundIn2Rows(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Long result = connectionSolver.findNumberOfSevens(new String[]{"abe abcdefg e d", "a abcdefg cbe e"});
      // Then
      assertThat(result, is(2L));
    }
  }

  @Nested
  @DisplayName("Finds connections combinations")
  class FindsConnectionsCombinations{

    @Test
    @DisplayName("Should contain 10 combinations in alphabetical order")
    void shouldFind10Combinations(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(results.size(), is(10));
    }

    @Test
    @DisplayName("Should find 0 using last 6 character combination which isn't 6 or 9")
    void shouldFind0(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "cagedb"), is(0));
    }


    @Test
    @DisplayName("Should find 1 using 2 characters")
    void shouldFind1(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "ab"), is(1));
    }

    @Test
    @DisplayName("Should find 2 using number 4's characters (2 characters match)")
    void shouldFind2(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "gcdfa"), is(2));
    }

    @Test
    @DisplayName("Should find 3 using number 1's characters (All characters match)")
    void shouldFind3(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "fbcad"), is(3));
    }

    @Test
    @DisplayName("Should find 4 using 4 characters")
    void shouldFind4(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "eafb"), is(4));
    }

    @Test
    @DisplayName("Should find 5 using last 5 character combination which isn't 2 or 3")
    void shouldFind5(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "cdfbe"), is(5));
    }

    @Test
    @DisplayName("Should find 6 using number 1's characters (1 characters match)\"")
    void shouldFind6() {
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "cdfgeb"), is(6));
    }

    @Test
    @DisplayName("Should find 7 using 3 characters")
    void shouldFind7(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "dab"), is(7));
    }

    @Test
    @DisplayName("Should find 8 using 7 characters")
    void shouldFind8(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "acedgfb"), is(8));
    }

    @Test
    @DisplayName("Should find 9 using number 4's characters (All characters match)")
    void shouldFind9(){
      // Given
      ConnectionSolver connectionSolver = new ConnectionSolver();
      // When
      Map<String, Integer> results = connectionSolver.findConnectionCombinations("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab");
      // Then
      assertThat(getNumberFromCombinationMap(results, "cefabd"), is(9));
    }



  }
}