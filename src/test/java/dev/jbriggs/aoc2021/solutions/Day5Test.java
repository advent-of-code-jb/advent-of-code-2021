package dev.jbriggs.aoc2021.solutions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.day5.Day5;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 5 tests")
class Day5Test {

  Day5 day5;

  @BeforeEach
  void beforeEach() {
    day5 = new Day5(new PuzzleInputParser());
  }

  @Nested
  @DisplayName("Part one tests")
  class PartOne {

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day5.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 1 when two lines intersect")
    void shouldReturn1When2LinesIntersect() {
      // Given
      String[] input = new String[] {"0,2 -> 5,2", "2,0 -> 2,5"};
      // When
      String result = day5.partOne(input);
      // Then
      assertThat(result, is("1"));
    }

    @Test
    @DisplayName("Should return 5 when test data entered")
    void shouldReturn5WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day5/input.txt");
      // When
      String result = day5.partOne(input);
      // Then
      assertThat(result, is("5"));
    }
  }

  @Nested
  @DisplayName("Part two tests")
  class PartTwo {

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day5.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 1 when two lines intersect")
    void shouldReturn1When2LinesIntersect() {
      // Given
      String[] input = new String[] {"0,2 -> 5,2", "2,0 -> 2,5"};
      // When
      String result = day5.partTwo(input);
      // Then
      assertThat(result, is("1"));
    }

    @Test
    @DisplayName("Should return 12 when test data entered")
    void shouldReturn12WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day5/input.txt");
      // When
      String result = day5.partTwo(input);
      // Then
      assertThat(result, is("12"));
    }
  }
}
