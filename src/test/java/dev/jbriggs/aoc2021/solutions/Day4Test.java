package dev.jbriggs.aoc2021.solutions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.day4.Day4;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 4 tests")
class Day4Test {

  Day4 day4;

  @BeforeEach
  void beforeEach() {
    day4 = new Day4(new PuzzleInputParser());
  }

  @Nested
  @DisplayName("Part One tests")
  class PartOne {

    @Test
    @DisplayName("Should return 0 when input empty")
    void shouldReturn0WhenInputEmpty() {
      // Given
      String[] input = new String[0];
      // When
      String result = day4.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 when just commands exist")
    void shouldReturn0WhenJustCommandsExist() {
      // Given
      String[] input = new String[] {"1,2,3,4,5"};
      // When
      String result = day4.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 when all unmarked numbers are zero")
    void shouldReturn0WhenAllUnmarkedNumbersAreZero() {
      // Given
      String[] input =
          new String[] {
            "1,2,3,4,5", "", "1 2 3 4 5", "0 0 0 0 0", "0 0 0 0 0", "0 0 0 0 0", "0 0 0 0 0"
          };
      // When
      String result = day4.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 5 when all unmarked numbers are zero apart from 1")
    void shouldReturn1WhenAllUnmarkedNumbersAreZeroApartFrom1() {
      // Given
      String[] input =
          new String[] {
              "6,2,3,4,5", "", "6 2 3 4 5", "1 0 0 0 0", "0 0 0 0 0", "0 0 0 0 0", "0 0 0 0 0"
          };
      // When
      String result = day4.partOne(input);
      // Then
      assertThat(result, is("5"));
    }

    @Test
    @DisplayName("Should return 4512 when using sample data")
    void shouldReturn4512WhenUsingSampleData() {
      // Given
      String[] input =
          new PuzzleInputParser().getPuzzleInputFromFile("testdata/day4/input.txt");
      // When
      String result = day4.partOne(input);
      // Then
      assertThat(result, is("4512"));
    }
  }

  @Nested
  @DisplayName("Part Two tests")
  class PartTwo{
    @Test
    @DisplayName("Should return 1924 when using sample data")
    void shouldReturn4512WhenUsingSampleData() {
      // Given
      String[] input =
          new PuzzleInputParser().getPuzzleInputFromFile("testdata/day4/input.txt");
      // When
      String result = day4.partTwo(input);
      // Then
      assertThat(result, is("1924"));
    }
  }
}
