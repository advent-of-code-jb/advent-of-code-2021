package dev.jbriggs.aoc2021.solutions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.day1.Day1;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 1 tests")
class Day1Test {

  private PuzzleInputParser puzzleInputParser;
  private Day1 solution;

  @BeforeEach
  void beforeEach() {
    puzzleInputParser = new PuzzleInputParser();
    solution = new Day1(puzzleInputParser);
  }

  @Nested
  @DisplayName("Part 1")
  class Day1TestPartOne {

    @Test
    @DisplayName("Should return 0 as answer when nothing entered")
    void shouldReturn0AsAnswerWhenNothingEntered() {
      // Given
      Integer[] input = new Integer[0];
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when 1 value entered")
    void shouldReturn0AsAnswerWhenOneValueEntered() {
      // Given
      Integer[] input = {123};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when no increasing values")
    void shouldReturn0AsAnswerWhenNoIncreasingValues() {
      // Given
      Integer[] input = {1000, 100, 10};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when duplicated values")
    void shouldReturn0AsAnswerWhenDuplicateValues() {
      // Given
      Integer[] input = {1000, 1000};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 1 as answer when 1 increasing value")
    void shouldReturn1AsAnswerWhen1IncreasingValues() {
      // Given
      Integer[] input = {1000, 1001};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("1"));
    }

    @Test
    @DisplayName("Should return 2 as answer when 2 increasing values")
    void shouldReturn2AsAnswerWhen2IncreasingValues() {
      // Given
      Integer[] input = {1000, 1001, 1002};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("2"));
    }

    @Test
    @DisplayName("Should return 2 as answer when 2 increasing values and 1 decreasing value")
    void shouldReturn2AsAnswerWhen2IncreasingValuesAnd1DecreasingValue() {
      // Given
      Integer[] input = {1000, 1001, 999, 1003};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("2"));
    }

    @Test
    @DisplayName("Should return 2 as answer when 2 increasing values and 1 decreasing value v2")
    void shouldReturn2AsAnswerWhen2IncreasingValuesAnd1DecreasingValueV2() {
      // Given
      Integer[] input = {1000, 1100, 1050, 1075};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("2"));
    }

    @Test
    @DisplayName("Should return 7 as answer using sample data")
    void shouldReturn7AsAnswerUsingSampleData() {
      // Given
      Integer[] input = puzzleInputParser.getPuzzleInputFromFileInteger("testdata/day1/input.txt");
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("7"));
    }
  }

  @Nested
  @DisplayName("Part 2")
  class Day1TestPartTwo {
    @Test
    @DisplayName("Should return 0 as answer when nothing entered")
    void shouldReturn0AsAnswerWhenNothingEntered() {
      // Given
      Integer[] input = new Integer[0];
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when 3 values entered")
    void shouldReturn0AsAnswerWhenOneValueEntered() {
      // Given
      Integer[] input = {123, 1000, 100};
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when less than 3 values entered")
    void shouldReturn0AsAnswerWhenLessThan3ValuesEntered() {
      // Given
      Integer[] input = {100,100};
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when 4 values entered the same")
    void shouldReturn0AsAnswerWhenOneValueEnteredTheSame() {
      // Given
      Integer[] input = {100,100,100,100};
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when 4 values entered but 2 windows are the same")
    void shouldReturn0AsAnswerWhen4ValuesEnteredBut2WindowsAreTheSame() {
      // Given
      Integer[] input = {100,150,200,100}; // 450 & 450
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when 4 values entered but 2nd window decreases")
    void shouldReturn0AsAnswerWhen4ValuesEnteredBut2ndWindowDescreased() {
      // Given
      Integer[] input = {100,150,200,50}; // 450 & 400
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 1 as answer when 4 incrementing values are entered")
    void shouldReturn0AsAnswerWhenFourIncrementingValuesAreEntered() {
      // Given
      Integer[] input = {100,120,130,150};
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("1"));
    }

    @Test
    @DisplayName("Should return 5 as answer using sample data")
    void shouldReturn7AsAnswerUsingSampleData() {
      // Given
      Integer[] input = puzzleInputParser.getPuzzleInputFromFileInteger("testdata/day1/input.txt");
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("5"));
    }
  }
}
