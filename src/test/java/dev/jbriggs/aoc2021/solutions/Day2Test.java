package dev.jbriggs.aoc2021.solutions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.day2.Day2;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 2 tests")
class Day2Test {
  private PuzzleInputParser puzzleInputParser;
  private Day2 solution;

  @BeforeEach
  void beforeEach() {
    puzzleInputParser = new PuzzleInputParser();
    solution = new Day2(puzzleInputParser);
  }

  @Nested
  @DisplayName("Part 1")
  class Day2TestPartOne {

    @Test
    @DisplayName("Should return 0 as answer when nothing entered")
    void shouldReturn0AsAnswerWhenNothingEntered() {
      // Given
      String[] input = new String[0];
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 1 as answer when 'forward 1, down 1' entered")
    void shouldReturn0AsAnswerWhenForward1Entered() {
      // Given
      String[] input = new String[] {"forward 1", "down 1"};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("1"));
    }

    @Test
    @DisplayName("Should return -1 as answer when 'up 1, forward 1' entered")
    void shouldReturnNeg1AsAnswerWhenUp1Entered() {
      // Given
      String[] input = new String[] {"up 1", "forward 1"};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("-1"));
    }

    @Test
    @DisplayName("Should return 1 as answer when 'down 1, forward 1' entered")
    void shouldReturn1AsAnswerWhenUp1Entered() {
      // Given
      String[] input = new String[] {"down 1", "forward 1"};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("1"));
    }

    @Test
    @DisplayName("Should return 150 as answer using sample data")
    void shouldReturn7AsAnswerUsingSampleData() {
      // Given
      String[] input = puzzleInputParser.getPuzzleInputFromFile("testdata/day2/input.txt");
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("150"));
    }
  }

  @Nested
  @DisplayName("Part 2")
  class Day2TestPartTwo {

    @Test
    @DisplayName("Should return 0 as answer when nothing entered")
    void shouldReturn0AsAnswerWhenNothingEntered() {
      // Given
      String[] input = new String[0];
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when 'forward 1, down 1'")
    void shouldReturn0AsAnswerWhenForward1Entered() {
      // Given
      String[] input = new String[] {"forward 1", "down 1"};
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 4 as answer when 'down 2, down 2, forward 1'")
    void shouldReturn4AsAnswerWhenDown2Down2Forward1Entered() {
      // Given
      String[] input = new String[] {"down 2", "down 2", "forward 1"};
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("4"));
    }

    @Test
    @DisplayName("Should return 0 as answer when 'forward 1, up 2, up 2'")
    void shouldReturn0AsAnswerWhenForward1Up2Up2Entered() {
      // Given
      String[] input = new String[] {"forward 1", "up 2", "up 2"};
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 900 as answer using sample data")
    void shouldReturn7AsAnswerUsingSampleData() {
      // Given
      String[] input = puzzleInputParser.getPuzzleInputFromFile("testdata/day2/input.txt");
      // When
      String result = solution.partTwo(input);
      // Then
      assertThat(result, is("900"));
    }
  }
}
