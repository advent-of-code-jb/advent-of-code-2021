package dev.jbriggs.aoc2021.solutions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.day3.Day3;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 3 tests")
class Day3Test {
  private PuzzleInputParser puzzleInputParser;
  private Day3 solution;

  @BeforeEach
  void beforeEach() {
    puzzleInputParser = new PuzzleInputParser();
    solution = new Day3(puzzleInputParser);
  }

  @Nested
  @DisplayName("Part 1")
  class Day3TestPartOne {

    @Test
    @DisplayName("Should return 0 as answer when nothing entered")
    void shouldReturn0AsAnswerWhenNothingEntered() {

      // Given
      String[] input = new String[0];
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 1 as answer when '1, 1, 0, 0' entered (1 = 1)")
    void shouldReturn0AsAnswerWhen1_1_0_0() {
      // Given
      String[] input = new String[] {"1", "1", "0", "0"};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 0 as answer when '1, 0, 0' entered (0 = 0)")
    void shouldReturn0AsAnswerWhen1_0_0Entered() {
      // Given
      String[] input = new String[] {"1", "0", "0"};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 2 as answer when '11, 10, 00' entered (10 = 2)")
    void shouldReturn0AsAnswerWhen11_10_00Entered() {
      // Given
      String[] input = new String[] {"11", "10", "00"};
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("2"));
    }

    @Test
    @DisplayName("Should return 198 as answer using sample data")
    void shouldReturn7AsAnswerUsingSampleData() {
      // Given
      String[] input = puzzleInputParser.getPuzzleInputFromFile("testdata/day3/input.txt");
      // When
      String result = solution.partOne(input);
      // Then
      assertThat(result, is("198"));
    }
  }

  @Nested
  @DisplayName("Part 2")
  class Day3TestPartTwo {

    @Nested
    @DisplayName("Oxygen generator rating")
    class OxygenGeneratorRating {

      @Test
      @DisplayName("Should return 1 when 1 entered")
      void shouldReturn1When1Entered() {
        // Given
        String[] input = new String[] {"1"};
        // When
        String result = solution.oxygenGeneratorRating(input);
        // Then
        assertThat(result, is("1"));
      }

      @Test
      @DisplayName("Should return 101 when 101 entered")
      void shouldReturn101When101Entered() {
        // Given
        String[] input = new String[] {"101"};
        // When
        String result = solution.oxygenGeneratorRating(input);
        // Then
        assertThat(result, is("101"));
      }

      @Test
      @DisplayName("Should return 111 when 101, 000, 111 entered")
      void shouldReturn111When111_001_111Entered() {
        // Given
        String[] input = new String[] {"101", "000", "111"};
        // When
        String result = solution.oxygenGeneratorRating(input);
        // Then
        assertThat(result, is("111"));
      }

      @Test
      @DisplayName("Should return 10111 with sample data")
      void shouldReturn1011WithSampleData() {
        // Given
        String[] input = puzzleInputParser.getPuzzleInputFromFile("testdata/day3/input.txt");
        // When
        String result = solution.oxygenGeneratorRating(input);
        // Then
        assertThat(result, is("10111"));
      }
    }

    @Nested
    @DisplayName("CO2 scrubber rating")
    class Co2ScrubberRating {

      @Test
      @DisplayName("Should return 1 when 1 entered")
      void shouldReturn1When1Entered() {
        // Given
        String[] input = new String[] {"1"};
        // When
        String result = solution.co2ScrubberRating(input);
        // Then
        assertThat(result, is("1"));
      }

      @Test
      @DisplayName("Should return 101 when 101 entered")
      void shouldReturn101When101Entered() {
        // Given
        String[] input = new String[] {"101"};
        // When
        String result = solution.co2ScrubberRating(input);
        // Then
        assertThat(result, is("101"));
      }

      @Test
      @DisplayName("Should return 001 when 101, 001, 111 entered")
      void shouldReturn111When111_001_111Entered() {
        // Given
        String[] input = new String[] {"101", "001", "111"};
        // When
        String result = solution.co2ScrubberRating(input);
        // Then
        assertThat(result, is("001"));
      }

      @Test
      @DisplayName("Should return 01010 with sample data")
      void shouldReturn01010WithSampleData() {
        // Given
        String[] input = puzzleInputParser.getPuzzleInputFromFile("testdata/day3/input.txt");
        // When
        String result = solution.co2ScrubberRating(input);
        // Then
        assertThat(result, is("01010"));
      }
    }
  }

  @Test
  @DisplayName("Should return 230 with sample data")
  void shouldReturn230WithSampleData() {
    // Given
    String[] input = puzzleInputParser.getPuzzleInputFromFile("testdata/day3/input.txt");
    // When
    String result = solution.partTwo(input);
    // Then
    assertThat(result, is("230"));
  }
}
