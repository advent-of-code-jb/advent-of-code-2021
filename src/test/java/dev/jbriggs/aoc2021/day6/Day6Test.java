package dev.jbriggs.aoc2021.day6;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 6 tests")
class Day6Test {

  Day6 day;

  @BeforeEach
  void beforeEach() {
    day = new Day6(new PuzzleInputParser());
  }

  @Nested
  @DisplayName("Part one tests")
  class PartOneTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return answer greater than 1 when 1 fish exists")
    void shouldReturnAnswerWhenFishExist() {
      // Given
      String[] input = new String[] {"1"};
      // When
      String result = day.partOne(input);
      // Then
      assertThat(Integer.parseInt(result), is(greaterThan(1)));
    }

    @Test
    @DisplayName("Should return 5934 when test data entered")
    void shouldReturn5934WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day6/input.txt");
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("5934"));
    }

  }

  @Nested
  @DisplayName("Part two tests")
  class PartTwoTests {

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return answer greater than 1 when 1 fish exists")
    void shouldReturnAnswerWhenFishExist() {
      // Given
      String[] input = new String[] {"1"};
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(Long.parseLong(result), is(greaterThan(1L)));
    }

    @Test
    @DisplayName("Should return 26984457539 when test data entered")
    void shouldReturn26984457539WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day6/input.txt");
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("26984457539"));
    }
  }
}
