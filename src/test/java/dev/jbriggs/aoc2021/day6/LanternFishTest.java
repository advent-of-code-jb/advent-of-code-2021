package dev.jbriggs.aoc2021.day6;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 6 - Lantern fish tests")
class LanternFishTest {

  @Nested
  @DisplayName("Should keep internal timer")
  class ShouldKeepInternalTimerTest{

    @Test
    @DisplayName("Should store internal timer on initialise")
    void shouldStoreInternalTimerOnInitialise(){
      // Given
      LanternFish lanternFish = new LanternFish(3);
      // When
      int result = lanternFish.getTimer();
      // Then
      assertThat(result, is(3));
    }

    @Test
    @DisplayName("Should store internal timer as 8 when no initial time provided")
    void shouldStoreInternalTimerOnInitialisePlus2Days(){
      // Given
      LanternFish lanternFish = new LanternFish();
      // When
      int result = lanternFish.getTimer();
      // Then
      assertThat(result, is(8));
    }

  }

  @Nested
  @DisplayName("Should update timer")
  class ShouldUpdateTimer{

    @Test
    @DisplayName("Should decrement timer by one on update")
    void shouldDecrementTimerByOneOnUpdate(){
      // Given
      LanternFish lanternFish = new LanternFish(3);
      // When
      lanternFish.update();
      // Then
      int result = lanternFish.getTimer();
      assertThat(result, is(2));
    }

    @Test
    @DisplayName("Should decrement timer back to 6 when going below 0")
    void shouldResetTimerBackToOriginalValue(){
      // Given
      LanternFish lanternFish = new LanternFish(1);
      // When
      lanternFish.update();
      lanternFish.update();
      // Then
      int result = lanternFish.getTimer();
      assertThat(result, is(6));
    }

    @Test
    @DisplayName("Should return FALSE when not giving birth")
    void shouldReturnFalseWhenNotGivingBirth(){
      // Given
      LanternFish lanternFish = new LanternFish(3);
      // When
      Boolean update = lanternFish.update();
      // Then
      int result = lanternFish.getTimer();
      assertThat(result, is(2));
      assertThat(update, is(false));
    }

    @Test
    @DisplayName("Should return TRUE when giving birth")
    void shouldReturnTrueWhenGivingBirth(){
      // Given
      LanternFish lanternFish = new LanternFish(1);
      // When
      Boolean update = lanternFish.update();
      Boolean update2 = lanternFish.update();
      // Then
      int result = lanternFish.getTimer();
      assertThat(result, is(6));
      assertThat(update, is(false));
      assertThat(update2, is(true));
    }
  }
}