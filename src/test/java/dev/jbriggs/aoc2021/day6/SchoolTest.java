package dev.jbriggs.aoc2021.day6;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.day6.School.LanternFishPair;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 6 - School tests")
class SchoolTest {

  @Nested
  @DisplayName("Should keep list of lantern fish")
  class ShouldKeepListOfLanternFishTest {

    @Test
    @DisplayName("Should keep list of lantern fish")
    void shouldKeepListOfLanternFish() {
      // Given
      LanternFish lanternFish = new LanternFish(3);
      School school = new School();
      // When
      school.addLanternFish(lanternFish);
      // Then
      assertThat(school.getLanternFishList(), hasItem(new LanternFishPair(lanternFish, 1L)));
    }

    @Test
    @DisplayName("Should increment map when fish with same counter added")
    void shouldIncrementMapWhenFishWithSameCounterAdded() {
      // Given
      LanternFish lanternFish = new LanternFish(3);
      School school = new School();
      // When
      school.addLanternFish(lanternFish);
      school.addLanternFish(lanternFish);
      school.addLanternFish(lanternFish);
      school.addLanternFish(lanternFish);
      // Then
      assertThat(school.getLanternFishList().size(), is(1));
      assertThat(school.getLanternFishList().get(0).getTotal(), is(4L));
    }
  }

  @Nested
  @DisplayName("Should be able to update fish in school")
  class ShouldBeAbleToUpdateFishTest {

    @Test
    @DisplayName("Should update fish")
    void shouldUpdateFish() {
      // Given
      LanternFish lanternFish = new LanternFish(3);
      School school = new School();
      school.addLanternFish(lanternFish);
      // When
      school.update(1);
      // Then
      assertThat(lanternFish.getTimer(), is(2));
    }

    @Test
    @DisplayName("Should update fish across 2 days")
    void shouldUpdateFishAcross2Days() {
      // Given
      LanternFish lanternFish = new LanternFish(3);
      School school = new School();
      school.addLanternFish(lanternFish);
      // When
      school.update(2);
      // Then
      assertThat(lanternFish.getTimer(), is(1));
    }

    @Test
    @DisplayName("Should update efficiently")
    void shouldUpdateEfficiently() {
      // Given
      LanternFish lanternFish = new LanternFish(3);
      LanternFish lanternFish2 = new LanternFish(5);
      School school = new School();
      school.addLanternFish(lanternFish);
      school.addLanternFish(lanternFish2);
      // When
      LocalTime before = LocalTime.now();
      school.update(256);
      LocalTime after = LocalTime.now();
      // Then
      assertThat(before.until(after, ChronoUnit.SECONDS), is(lessThan(5L)));
      assertThat(school.getTotalFish(), is(9585455251L));
    }

    @Test
    @DisplayName("Should add new fish when current fish breeds")
    void shouldAddNewFishWhenCurrentFishBreeds() {
      // Given
      LanternFish lanternFish = new LanternFish(1);
      School school = new School();
      school.addLanternFish(lanternFish);
      // When
      school.update(2);
      // Then
      assertThat(school.getTotalFish(), is(2L));
      assertThat(school.getLanternFishList(), hasItem(new LanternFishPair(new LanternFish(8), 1L)));
    }
  }

  @Nested
  @DisplayName("Should be able to get total fish in school")
  class ShouldBeAbleToGetTotalFishInSchoolTest {

    @Test
    @DisplayName("Should return total fish in school")
    void shouldReturnTotalFishInSchool() {
      // Given
      LanternFish lanternFish = new LanternFish(3);
      School school = new School();
      school.addLanternFish(lanternFish);
      // When
      Long totalFish = school.getTotalFish();
      // Then
      assertThat(totalFish, is(1L));
    }
  }

  @Test
  @DisplayName("Should return 26 when test data entered")
  void shouldReturn26WhenTestDataEntered() {
    // Given
    String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day6/input.txt");
    School school = new School();
    for(String number : input[0].split(",")){
      school.addLanternFish(new LanternFish(Integer.parseInt(number)));
    }
    // When
    school.update(18);
    Long result = school.getTotalFish();
    // Then
    assertThat(result, is(26L));
  }

  @Test
  @DisplayName("Should return 5934 when test data entered")
  void shouldReturn5934WhenTestDataEntered() {
    // Given
    String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day6/input.txt");
    School school = new School();
    for(String number : input[0].split(",")){
      school.addLanternFish(new LanternFish(Integer.parseInt(number)));
    }
    // When
    school.update(80);
    Long result = school.getTotalFish();
    // Then
    assertThat(result, is(5934L));
  }
}
