package dev.jbriggs.aoc2021.day11;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 11 - Octopus collection tests")
class OctopusCollectionTest {

  @Nested
  @DisplayName("Can store octopus in collection tests")
  class CanStore100OctopusInCollection{

    @Test
    @DisplayName("Should be able to store octopus")
    void shouldBeAbleToStoreOctopus(){
      // Given
      String[] input = new String[]{"11111", "19991", "19191", "19991", "11111"};
      // When
      OctopusCollection collection = new OctopusCollection(input);
      // Then
      assertThat(collection.getWidth(), is(5));
      assertThat(collection.getHeight(), is(5));
    }
  }

  @Nested
  @DisplayName("Can get octopus at position tests")
  class CanGetOctopusAtPosition{

    @Test
    @DisplayName("Should return empty if octopus does not exist")
    void shouldReturnEmptyIfOctopusDoesNotExist(){
      // Given
      String[] input = new String[]{"11111", "19991", "19191", "19991", "11111"};
      OctopusCollection collection = new OctopusCollection(input);
      // When
      Optional<OctopusCoordinate> octopusEnergy = collection.getOctopusAtPosition(1000, 1000);
      // Then
      assertThat(octopusEnergy.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Should return empty if x >= width")
    void shouldReturnEmptyIfOctopusIfXGreaterThanRange(){
      // Given
      String[] input = new String[]{"11111", "19991", "19191", "19991", "11111"};
      OctopusCollection collection = new OctopusCollection(input);
      // When
      Optional<OctopusCoordinate> octopusEnergy = collection.getOctopusAtPosition(5, 1);
      // Then
      assertThat(octopusEnergy.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Should return empty if y >= height")
    void shouldReturnEmptyIfOctopusIfYGreaterThanRange(){
      // Given
      String[] input = new String[]{"11111", "19991", "19191", "19991", "11111"};
      OctopusCollection collection = new OctopusCollection(input);
      // When
      Optional<OctopusCoordinate> octopusEnergy = collection.getOctopusAtPosition(1, 5);
      // Then
      assertThat(octopusEnergy.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Should return octopus energy if exists")
    void shouldReturnOctopusEnergyIfExists(){
      // Given
      String[] input = new String[]{
          "12345",
          "67890"};
      OctopusCollection collection = new OctopusCollection(input);
      // When
      Optional<OctopusCoordinate> octopusEnergy = collection.getOctopusAtPosition(1, 0);
      // Then
      assertThat(octopusEnergy.isEmpty(), is(false));
      assertThat(octopusEnergy.get().getEnergyLevel(), is(7));
      assertThat(octopusEnergy.get().getX(), is(1));
      assertThat(octopusEnergy.get().getY(), is(0));
    }
  }

  @Nested
  @DisplayName("Should increase octopus energy on step tests")
  class shouldIncreaseOctopusEnergyOnStep{

    @Test
    @DisplayName("Should increase octopus energy by 1 after step")
    void shouldIncreaseOcotpusEnergyBy1AfterStep(){
      // Given
      String[] input = new String[]{"1"};
      OctopusCollection collection = new OctopusCollection(input);
      // When
      collection.step();
      Optional<OctopusCoordinate> octopusEnergy = collection.getOctopusAtPosition(0, 0);
      // Then
      assertThat(octopusEnergy.isEmpty(), is(false));
      assertThat(octopusEnergy.get().getEnergyLevel(), is(2));
    }

    @Test
    @DisplayName("Should increase octopus energy and reset to 0 when 9")
    void shouldIncreaseOcotpusEnergyBy1AfterStepAndResetTo0When9(){
      // Given
      String[] input = new String[]{"9"};
      OctopusCollection collection = new OctopusCollection(input);
      // When
      collection.step();
      Optional<OctopusCoordinate> octopusEnergy = collection.getOctopusAtPosition(0, 0);
      // Then
      assertThat(octopusEnergy.isEmpty(), is(false));
      assertThat(octopusEnergy.get().getEnergyLevel(), is(0));
    }

    @Nested
    @DisplayName("Should increase neighbours when reset")
    class shouldIncreaseNeighboursWhenReset{

      @Test
      @DisplayName("Should increase all neighbours by 1 when reset back to 0")
      void shouldIncreaseAllNeighboursBy1WhenResetBackTo0(){
        // Given
        String[] input = new String[]{"999", "919", "999"};
        OctopusCollection collection = new OctopusCollection(input);
        // When
        collection.step();
        // Then
        for(int x = 0; x < 3; x++){
          for(int y = 0; y < 3; y++){
            assertThat(collection.getOctopus()[x][y].getEnergyLevel(), is(0));
          }
        }
      }
    }
  }
}