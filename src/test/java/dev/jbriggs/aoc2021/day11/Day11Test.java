package dev.jbriggs.aoc2021.day11;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 11 tests")
class Day11Test {
  Day11 day;

  @BeforeEach
  void beforeEach() {
    day = new Day11(new PuzzleInputParser());
  }

  @Nested
  @DisplayName("Part one tests")
  class PartOneTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 1656 when using test input")
    void shouldReturn1656WhenUsingTestInput() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day11/input.txt");
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("1656"));
    }

  }

  @Nested
  @DisplayName("Part two tests")
  class PartTwoTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 195 when using test input")
    void shouldReturn195WhenUsingTestInput() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day11/input.txt");
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("195"));
    }

  }
}