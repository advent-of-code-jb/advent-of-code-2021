package dev.jbriggs.aoc2021.day10;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 10 - Score calculator tests")
class ScoreCalculatorTest {

  @Nested
  @DisplayName("Should return correct corrupted score tests")
  class ShouldReturnCorrectCorruptedScoreTests {

    @Test
    @DisplayName("Should throw exception if unrecognized character entered")
    void shouldReturn0IfRandomCharacterEntered() throws AdventOfCodeException {
      // Given
      char character = 'a';
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                ScoreCalculator.getCorruptedScore(character);
              });
      // Then
      assertThat(exception.getMessage(), is("Invalid character passed into score calculator"));
    }

    @Test
    @DisplayName("Should return 3 if round closing bracket ')'")
    void shouldReturn3IfRoundClosingBracket() throws AdventOfCodeException {
      // Given
      char character = ')';
      // When
      long result = ScoreCalculator.getCorruptedScore(character);
      // Then
      assertThat(result, is(3L));
    }

    @Test
    @DisplayName("Should return 57 if square closing bracket ']'")
    void shouldReturn57IfSquareClosingBracket() throws AdventOfCodeException {
      // Given
      char character = ']';
      // When
      long result = ScoreCalculator.getCorruptedScore(character);
      // Then
      assertThat(result, is(57L));
    }

    @Test
    @DisplayName("Should return 1197 if curly closing bracket '}'")
    void shouldReturn1197IfSquareClosingBracket() throws AdventOfCodeException {
      // Given
      char character = '}';
      // When
      long result = ScoreCalculator.getCorruptedScore(character);
      // Then
      assertThat(result, is(1197L));
    }

    @Test
    @DisplayName("Should return 25137 if angle closing bracket '>'")
    void shouldReturn25137IfAngleClosingBracket() throws AdventOfCodeException {
      // Given
      char character = '>';
      // When
      long result = ScoreCalculator.getCorruptedScore(character);
      // Then
      assertThat(result, is(25137L));
    }
  }

  @Nested
  @DisplayName("Should return correct incomplete score tests")
  class ShouldReturnCorrectIncompleteScoreTests {

    @Test
    @DisplayName("Should throw exception if unrecognized character entered")
    void shouldReturn0IfRandomCharacterEntered() throws AdventOfCodeException {
      // Given
      char character = 'a';
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                ScoreCalculator.getIncompleteScore(character);
              });
      // Then
      assertThat(exception.getMessage(), is("Invalid character passed into score calculator"));
    }

    @Test
    @DisplayName("Should return 1 if round closing bracket ')'")
    void shouldReturn1IfRoundClosingBracket() throws AdventOfCodeException {
      // Given
      char character = ')';
      // When
      long result = ScoreCalculator.getIncompleteScore(character);
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 if square closing bracket ']'")
    void shouldReturn2IfSquareClosingBracket() throws AdventOfCodeException {
      // Given
      char character = ']';
      // When
      long result = ScoreCalculator.getIncompleteScore(character);
      // Then
      assertThat(result, is(2L));
    }

    @Test
    @DisplayName("Should return 3 if curly closing bracket '}'")
    void shouldReturn3IfSquareClosingBracket() throws AdventOfCodeException {
      // Given
      char character = '}';
      // When
      long result = ScoreCalculator.getIncompleteScore(character);
      // Then
      assertThat(result, is(3L));
    }

    @Test
    @DisplayName("Should return 4 if angle closing bracket '>'")
    void shouldReturn4IfAngleClosingBracket() throws AdventOfCodeException {
      // Given
      char character = '>';
      // When
      long result = ScoreCalculator.getIncompleteScore(character);
      // Then
      assertThat(result, is(4L));
    }
  }
}
