package dev.jbriggs.aoc2021.day10;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 10 - Line reader tests")
class LineReaderTest {

  @Nested
  @DisplayName("Should read line and return corrupted score tests")
  class ShouldReadRowAndReturnCorruptedScoreTest {

    @Test
    @DisplayName("Should throw exception if illegal character in row")
    void shouldThrowExceptionIfIllegalCharacterInRow() {
      // Given
      String line = "([{ab}])";
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                LineReader.getCorruptedScore(line);
              });
      // Then
      assertThat(exception.getMessage(), is("Illegal character found in row"));
    }

    @Test
    @DisplayName("Should return 0 if legal line '([{<>}])'")
    void shouldReturn0IfLegalLine() {
      // Given
      String line = "([{<>}])";
      // When
      long result = LineReader.getCorruptedScore(line);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 if incomplete line '([{<>}'")
    void shouldReturn0IfIncompleteLine() {
      // Given
      String line = "([{<>}";
      // When
      long result = LineReader.getCorruptedScore(line);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 3 if corrupted line due to round bracket ')' '([{<>}))'")
    void shouldReturn3IfCorruptedLineRoundBracket() {
      // Given
      String line = "([{<>}))";
      // When
      long result = LineReader.getCorruptedScore(line);
      // Then
      assertThat(result, is(3L));
    }

    @Test
    @DisplayName("Should return 1197 if corrupted line due to curly bracket '}' '{([(<{}[<>[]}>{[]{[(<()>'")
    void shouldReturn1197IfCorruptedLineDueToCurlyBracket() {
      // Given
      String line = "{([(<{}[<>[]}>{[]{[(<()>";
      // When
      long result = LineReader.getCorruptedScore(line);
      // Then
      assertThat(result, is(1197L));
    }
  }

  @Nested
  @DisplayName("Should read full input and return full corrupted score tests")
  class ShouldReadFullInputAndReturnCorruptedFullScore{

    @Test
    @DisplayName("Should return 0 when multiple legal lines")
    void shouldReturn0WhenMultipleLegalLines(){
      // Given
      String[] lines = {"([{<>}])", "([{<>}])"};
      // When
      long result = LineReader.getFullCorruptedScore(lines);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 when multiple incomplete lines")
    void shouldReturn0WhenMultipleIncompleLines(){
      // Given
      String[] lines = {"([{<>}", "([{<>}"};
      // When
      long result = LineReader.getFullCorruptedScore(lines);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 3 when one corrupted line")
    void shouldReturn3When1CorruptedLine(){
      // Given
      String[] lines = {"([{<>}))", "([{<>}"};
      // When
      long result = LineReader.getFullCorruptedScore(lines);
      // Then
      assertThat(result, is(3L));
    }
  }

  @Nested
  @DisplayName("Should read line and return incomplete score tests")
  class ShouldReadRowAndReturnIncompleteScoreTest{
    
    @Test
    @DisplayName("Should throw exception if illegal character in row")
    void shouldThrowExceptionIfIllegalCharacterInRow() {
      // Given
      String line = "([{ab}])";
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                LineReader.getIncompleteScore(line);
              });
      // Then
      assertThat(exception.getMessage(), is("Illegal character found in row"));
    }

    @Test
    @DisplayName("Should return 0 if legal line '([{<>}])'")
    void shouldReturn0IfLegalLine() {
      // Given
      String line = "([{<>}])";
      // When
      long result = LineReader.getIncompleteScore(line);
      // Then
      assertThat("Result should be 0 as line is complete", result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 if corrupted line '([{<}])'")
    void shouldReturn0IfCorruptedLine() {
      // Given
      String line = "([{<)}])";
      // When
      long result = LineReader.getIncompleteScore(line);
      // Then
      assertThat("Result should be 0 as line is corrupted (e.g. should be ignored)", result, is(0L));
    }

    @Test
    @DisplayName("Should return 1 if incomplete line '([{<>}]'")
    void shouldReturn3IfIncompleteLine() {
      // Given
      String line = "([{<>}]";
      // When
      long result = LineReader.getIncompleteScore(line);
      // Then
      assertThat("Result should be 1 as line is missing round bracket", result, is(1L));
    }

    @Test
    @DisplayName("Should return 294 if incomplete line ('])}>' missing) '<{([{{}}[<[[[<>{}]]]>[]]'")
    void shouldReturn60IfIncompleteLine() {
      // Given
      String line = "<{([{{}}[<[[[<>{}]]]>[]]";
      // When
      long result = LineReader.getIncompleteScore(line);
      // Then
      assertThat("Result should be 294 as line is missing '])}>' on end", result, is(294L));
    }

    @Test
    @DisplayName("Should return 288957 if incomplete line '[({(<(())[]>[[{[]{<()<>>'")
    void shouldReturn288957IfIncompleteLine() {
      // Given
      String line = "[({(<(())[]>[[{[]{<()<>>";
      // When
      long result = LineReader.getIncompleteScore(line);
      // Then
      assertThat("Result should be 3 as line is missing '}}]])})]' on end", result, is(288957L));
    }
  }

  @Nested
  @DisplayName("Should return full input and return middle incomplete score tests")
  class ShouldReadFullInputAndReturnMiddleIncompleteScore{
    @Test
    @DisplayName("Should return 0 when multiple legal lines")
    void shouldReturn0WhenMultipleLegalLines(){
      // Given
      String[] lines = {"([{<>}])", "([{<>}])"};
      // When
      long result = LineReader.getMiddleIncompleteScore(lines);
      // Then
      assertThat("Should return 0 as no lines are incomplete", result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 when multiple corrupted lines")
    void shouldReturn0WhenMultipleCorruptedLines(){
      // Given
      String[] lines = {"([{<>}))", "([{<>}))"};
      // When
      long result = LineReader.getMiddleIncompleteScore(lines);
      // Then
      assertThat("Should return 0 as no lines are incomplete", result, is(0L));
    }

    @Test
    @DisplayName("Should return 293 when one incomplete line")
    void shouldReturn293WhenOneMultipleCorruptedLines(){
      // Given
      String[] lines = {"<{([{{}}[<[[[<>{}]]]>[]]"};
      // When
      long result = LineReader.getMiddleIncompleteScore(lines);
      // Then
      assertThat("Should return 293 as one incomplete line", result, is(294L));
    }
  }
}
