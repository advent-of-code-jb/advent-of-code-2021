package dev.jbriggs.aoc2021.day14;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 14 - PolymerTemplate tests")
class PolymerTemplateTest {

  @Nested
  @DisplayName("Needs to store template")
  class NeedsToStoreTemplate {

    @Test
    @DisplayName("Should throw exception when empty string passed")
    void shouldThrowExceptionWhenEmptyStringPassed() {
      // Given
      String template = "";
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                new PolymerTemplate(template);
              });
      // Then
      assertThat(exception.getMessage(), is("Template must not be empty"));
    }

    @Test
    @DisplayName("Should store template")
    void shouldStoreTemplate() {
      // Given
      String template = "ABCD";
      // When
      PolymerTemplate polymerTemplate = new PolymerTemplate(template);

      // Then
      assertThat(polymerTemplate.getTemplatePairs().size(), is(3));
    }
  }

  @Nested
  @DisplayName("Needs to store pair insertion rules")
  class NeedsToStorePairInsertionRules{

    @Test
    @DisplayName("Should throw exception when empty rules list")
    void shouldThrowExceptionWhenEmptyRuleList(){
      // Given
      String template = "ABCD";
      List<PairInsertionRule> rules = new ArrayList<>();
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                new PolymerTemplate(template, rules);
              });
      // Then
      assertThat(exception.getMessage(), is("Rules list must contain rules"));
    }

    @Test
    @DisplayName("Should return rules when rules are added")
    void shouldReturnRulesWhenRulesAreAdded(){
      // Given
      String template = "ABCD";
      PairInsertionRule pairInsertionRule = new PairInsertionRule("AB -> C");
      List<PairInsertionRule> rules = Collections.singletonList(pairInsertionRule);
      // When
      PolymerTemplate polymerTemplate =
                new PolymerTemplate(template, rules);
      // Then
      assertThat(polymerTemplate.getRules(), hasItem(pairInsertionRule));
    }
  }
  @Nested
  @DisplayName("Needs to be able to apply rules")
  class NeedsToApplyRules{

    @Test
    @DisplayName("Should be able to apply rules")
    void shouldBeAbleToApplyRules(){
      // Given
      String template = "ABCD";
      PairInsertionRule pairInsertionRule = new PairInsertionRule("AB -> C");
      List<PairInsertionRule> rules = Collections.singletonList(pairInsertionRule);
      PolymerTemplate polymerTemplate =
          new PolymerTemplate(template, rules);
      // When
      polymerTemplate.step();
      // Then
      assertThat(polymerTemplate.getTemplatePairs().size(), is(4));
    }

    @Test
    @DisplayName("Should not apply rules to new characters")
    void shouldNotApplyRulesToNewCharacters(){
      // Given
      String template = "ABCD";
      PairInsertionRule pairInsertionRule = new PairInsertionRule("AB -> C");
      PairInsertionRule pairInsertionRule2 = new PairInsertionRule("CB -> B");
      List<PairInsertionRule> rules = Arrays.asList(pairInsertionRule, pairInsertionRule2);
      PolymerTemplate polymerTemplate =
          new PolymerTemplate(template, rules);
      // When
      polymerTemplate.step();
      // Then
      assertThat(polymerTemplate.getTemplatePairs().size(), is(4));
    }
  }

  @Nested
  @DisplayName("Get count of most occurring character tests")
  class getCountOfMostOccurringCharacter {

    @Test
    @DisplayName("Should return 1 when all characters is the same")
    void shouldReturn1WhenAllCharactersTheSame(){
      // Given
      String template = "ABCD";
      PairInsertionRule pairInsertionRule = new PairInsertionRule("AB -> C");
      PairInsertionRule pairInsertionRule2 = new PairInsertionRule("CB -> B");
      List<PairInsertionRule> rules = Arrays.asList(pairInsertionRule, pairInsertionRule2);
      PolymerTemplate polymerTemplate =
          new PolymerTemplate(template, rules);
      // When
      Long result = polymerTemplate.countMostOccuringCharacter();
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 2 when two characters is the same")
    void shouldReturn2WhenAllCharactersTheSame(){
      // Given
      String template = "ABCA";
      PairInsertionRule pairInsertionRule = new PairInsertionRule("AB -> C");
      PairInsertionRule pairInsertionRule2 = new PairInsertionRule("CB -> B");
      List<PairInsertionRule> rules = Arrays.asList(pairInsertionRule, pairInsertionRule2);
      PolymerTemplate polymerTemplate =
          new PolymerTemplate(template, rules);
      // When
      Long result = polymerTemplate.countMostOccuringCharacter();
      // Then
      assertThat(result, is(2L));
    }
  }

  @Nested
  @DisplayName("Get count of least occurring character tests")
  class getCountOfLeastOccurringCharacter {

    @Test
    @DisplayName("Should return 1 when all characters is the same")
    void shouldReturn1WhenAllCharactersTheSame(){
      // Given
      String template = "ABCD";
      PairInsertionRule pairInsertionRule = new PairInsertionRule("AB -> C");
      PairInsertionRule pairInsertionRule2 = new PairInsertionRule("CB -> B");
      List<PairInsertionRule> rules = Arrays.asList(pairInsertionRule, pairInsertionRule2);
      PolymerTemplate polymerTemplate =
          new PolymerTemplate(template, rules);
      // When
      Long result = polymerTemplate.countLeastOccuringCharacter();
      // Then
      assertThat(result, is(1L));
    }

    @Test
    @DisplayName("Should return 1 when one unique character")
    void shouldReturn2WhenAllCharactersTheSame(){
      // Given
      String template = "ABBB";
      PairInsertionRule pairInsertionRule = new PairInsertionRule("AB -> C");
      PairInsertionRule pairInsertionRule2 = new PairInsertionRule("CB -> B");
      List<PairInsertionRule> rules = Arrays.asList(pairInsertionRule, pairInsertionRule2);
      PolymerTemplate polymerTemplate =
          new PolymerTemplate(template, rules);
      // When
      Long result = polymerTemplate.countLeastOccuringCharacter();
      // Then
      assertThat(result, is(1L));
    }
  }
}
