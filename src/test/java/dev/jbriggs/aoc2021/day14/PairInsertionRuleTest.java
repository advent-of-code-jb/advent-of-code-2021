package dev.jbriggs.aoc2021.day14;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 14 - PairInsertionRule tests")
class PairInsertionRuleTest {

  @Nested
  @DisplayName("Needs to store rule")
  class NeedsToStoreRule{

    @Test
    @DisplayName("Should throw exception when empty string passed to constructor")
    void shouldThrowExceptionWhenEmptyStringPassedToConstructor(){
      // Given
      String rawRule = "";
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                new PairInsertionRule(rawRule);
              });
      // Then
      assertThat(exception.getMessage(), is("Wrong pattern inserted"));
    }

    @Test
    @DisplayName("Should save element pair and result element")
    void shouldSaveElementPairAndResultElement(){
      // Given
      String rawRule = "AB -> C";
      // When
      PairInsertionRule pairInsertionRule = new PairInsertionRule(rawRule);
      // Then
      assertThat(pairInsertionRule.getRule(), is("AB"));
      assertThat(pairInsertionRule.getResult(), is("C"));
    }
  }
}