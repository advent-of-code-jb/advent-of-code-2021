package dev.jbriggs.aoc2021.day13;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import dev.jbriggs.aoc2021.common.Coordinate;
import dev.jbriggs.aoc2021.day13.PaperFolder.Fold;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 13 - Paper folder test")
class PaperFolderTest {

  @Nested
  @DisplayName("Should be able to take in dot input")
  class ShouldBeAbleToTakeInDotInput {

    @Test
    @DisplayName("Should throw exception when input is empty")
    void shouldThrowExceptionWhenInputIsEmpty() {
      // Given
      String[] rawDots = {};
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                new PaperFolder(rawDots, new String[] {"fold along y=6"});
              });
      // Then
      assertThat(exception.getMessage(), is("Dots input must not be empty"));
    }

    @Test
    @DisplayName("Should throw exception when wrong format is used")
    void shouldThrowExceptionWhenWrongFormatIsUsed() {
      // Given
      String[] rawDots = {"10 - 20"};
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                new PaperFolder(rawDots, new String[] {"fold along y=6"});
              });
      // Then
      assertThat(exception.getMessage(), is("Wrong format used, should be ^(\\d+),(\\d+)$"));
    }

    @Test
    @DisplayName("Should save dot input when correct format used")
    void shouldSaveDotInputWhenCorrectFormatUsed() {
      // Given
      String[] rawDots = {"10,20"};
      // When
      PaperFolder paperFolder = new PaperFolder(rawDots, new String[] {"fold along y=6"});
      // Then
      assertThat(paperFolder.getPaper().getDots(), hasItem(new Coordinate(10, 20)));
    }
  }

  @Nested
  @DisplayName("Should be able to take in fold input")
  class ShouldBeAbleToTakeInFoldInput {
    @Test
    @DisplayName("Should throw exception when input is empty")
    void shouldThrowExceptionWhenInputIsEmpty() {
      // Given
      String[] rawFolds = {};
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                new PaperFolder(new String[] {"0,0"}, rawFolds);
              });
      // Then
      assertThat(exception.getMessage(), is("Fold input must not be empty"));
    }

    @Test
    @DisplayName("Should throw exception when wrong format is used")
    void shouldThrowExceptionWhenWrongFormatIsUsed() {
      // Given
      String[] rawDots = {"10,20"};
      String[] rawFolds = {"fold along y - 6"};
      // When
      AdventOfCodeException exception =
          assertThrows(
              AdventOfCodeException.class,
              () -> {
                new PaperFolder(rawDots, rawFolds);
              });
      // Then
      assertThat(
          exception.getMessage(), is("Wrong format used, should be ^fold along (\\w+)=(\\d+)$"));
    }

    @Test
    @DisplayName("Should save fold input when correct input used")
    void shouldSaveFoldInputWhenCorrectInputUsed() {
      // Given
      String[] rawDots = {"10,20"};
      String[] rawFolds = {"fold along y=6"};
      // When

      PaperFolder paperFolder = new PaperFolder(rawDots, rawFolds);

      // Then
      assertThat(paperFolder.getFolds(), hasItem(new Fold("y", 6)));
    }
  }

  @Nested
  @DisplayName("Should be able to fold")
  class shouldBeAbleToFold {
    @Test
    @DisplayName("Should be able to fold on x=1")
    void shouldSaveFoldInputWhenCorrectXInputUsed() {
      // Given
      String[] rawDots = {"0,0", "2,0"};
      String[] rawFolds = {"fold along x=1"};
      PaperFolder paperFolder = new PaperFolder(rawDots, rawFolds);
      // When
      paperFolder.startFold();
      // Then
      assertThat(paperFolder.getPaper().getDots().size(), is(1));
      assertThat(paperFolder.getPaper().getDots(), hasItem(new Coordinate(0,0)));
    }

    @Test
    @DisplayName("Should be able to fold on y=1")
    void shouldSaveFoldInputWhenCorrectYInputUsed() {
      // Given
      String[] rawDots = {"0,0", "0,2"};
      String[] rawFolds = {"fold along y=1"};
      PaperFolder paperFolder = new PaperFolder(rawDots, rawFolds);
      // When
      paperFolder.startFold();
      // Then
      assertThat(paperFolder.getPaper().getDots().size(), is(1));
      assertThat(paperFolder.getPaper().getDots(), hasItem(new Coordinate(0,0)));
    }
  }
}
