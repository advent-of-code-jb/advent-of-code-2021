package dev.jbriggs.aoc2021.day13;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import dev.jbriggs.aoc2021.common.Coordinate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 13 - Transparent paper test")
class TransparentPaperTest {

  @Nested
  @DisplayName("Should be able to store dots on paper tests")
  class ShouldBeAbleToStoreDotsOnPaper {

    @Test
    @DisplayName("Should be able to store single dot")
    void shouldStoreSingleDot() {
      // Given
      Coordinate coordinate = new Coordinate(0, 0);
      // When
      TransparentPaper transparentPaper = new TransparentPaper(new Coordinate[] {coordinate});
      // Then
      assertThat(transparentPaper.getDots().size(), is(1));
      assertThat(transparentPaper.getDots(), hasItem(coordinate));
    }

    @Test
    @DisplayName("Should be able to store multiple dots")
    void shouldStoreMultipleDots() {
      // Given
      Coordinate coordinate = new Coordinate(0, 0);
      Coordinate coordinate2 = new Coordinate(1, 0);
      // When
      TransparentPaper transparentPaper =
          new TransparentPaper(new Coordinate[] {coordinate, coordinate2});
      // Then
      assertThat(transparentPaper.getDots().size(), is(2));
      assertThat(transparentPaper.getDots(), hasItem(coordinate));
      assertThat(transparentPaper.getDots(), hasItem(coordinate2));
    }

    @Test
    @DisplayName("Should be able to store single dot when same coordinate added")
    void shouldStoreSingleDotWhenDotOfSameCoordinateAdded() {
      // Given
      Coordinate coordinate = new Coordinate(0, 0);
      Coordinate coordinate2 = new Coordinate(0, 0);
      // When;
      TransparentPaper transparentPaper =
          new TransparentPaper(new Coordinate[] {coordinate, coordinate2});

      // Then
      assertThat(transparentPaper.getDots().size(), is(1));
      assertThat(transparentPaper.getDots(), hasItem(coordinate));
    }
  }

  @Nested
  @DisplayName("Should be able to store paper dimensions test")
  class ShouldBeAbleToStorePaperDimensions {

    @Nested
    @DisplayName("Get Width")
    class getWidth {

      @Test
      @DisplayName("Should return Width of 1 when one coordinate")
      void shouldReturnXDimension() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        // When
        TransparentPaper transparentPaper = new TransparentPaper(new Coordinate[] {coordinate});
        // Then
        assertThat(transparentPaper.getWidth(), is(1));
      }

      @Test
      @DisplayName("Should return Width of 2 when one coordinate with x = 1")
      void shouldReturnWidthOf2() {
        // Given
        Coordinate coordinate = new Coordinate(1, 0);
        // When
        TransparentPaper transparentPaper = new TransparentPaper(new Coordinate[] {coordinate});
        // Then
        assertThat(transparentPaper.getWidth(), is(2));
      }
    }

    @Nested
    @DisplayName("Get Height")
    class getHeight {

      @Test
      @DisplayName("Should return Height of 1 when one coordinate")
      void shouldReturnHeight() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        // When
        TransparentPaper transparentPaper = new TransparentPaper(new Coordinate[] {coordinate});
        // Then
        assertThat(transparentPaper.getHeight(), is(1));
      }

      @Test
      @DisplayName("Should return Width of 2 when one coordinate with y = 1")
      void shouldReturnWidthOf2() {
        // Given
        Coordinate coordinate = new Coordinate(0, 1);
        // When
        TransparentPaper transparentPaper = new TransparentPaper(new Coordinate[] {coordinate});
        // Then
        assertThat(transparentPaper.getHeight(), is(2));
      }
    }
  }

  @Nested
  @DisplayName("Should be able to fold")
  class ShouldBeAbleToFold {

    @Nested
    @DisplayName("On X")
    class OnX {

      @Test
      @DisplayName("Should throw exception when can't fold on X dimension")
      void shouldThrowExceptionWhenCantFoldOnXDimension() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        TransparentPaper transparentPaper = new TransparentPaper(new Coordinate[] {coordinate});
        // When
        AdventOfCodeException exception =
            assertThrows(
                AdventOfCodeException.class,
                () -> {
                  transparentPaper.foldOnX(10);
                });
        // Then
        assertThat(exception.getMessage(), is("Fold exceeds papers width"));
      }

      @Test
      @DisplayName("Should fold and merge coordinates when folding")
      void shouldFoldAndMergeCoordinatesWhenFolding() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        Coordinate coordinate2 = new Coordinate(2, 0);
        TransparentPaper transparentPaper =
            new TransparentPaper(new Coordinate[] {coordinate, coordinate2});
        // When
        transparentPaper.foldOnX(1);
        // Then
        assertThat(transparentPaper.getDots().size(), is(1));
        assertThat(transparentPaper.getDots(), hasItem(coordinate));
      }

      @Test
      @DisplayName("Should fold and merge coordinates when folding on wider paper")
      void shouldFoldAndMergeCoordinatesWhenFoldingOnWiderPaper() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        Coordinate coordinate2 = new Coordinate(6, 0);
        TransparentPaper transparentPaper =
            new TransparentPaper(new Coordinate[] {coordinate, coordinate2});
        // When
        transparentPaper.foldOnX(3);
        // Then
        assertThat(transparentPaper.getDots().size(), is(1));
        assertThat(transparentPaper.getDots(), hasItem(coordinate));
      }

      @Test
      @DisplayName(
          "Should fold and merge coordinates when folding on wider paper and keep non overlapping dot")
      void shouldFoldAndMergeCoordinatesWhenFoldingOnWiderPaperAndKeepNonOverlappingDot() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        Coordinate coordinate2 = new Coordinate(5, 0);
        TransparentPaper transparentPaper =
            new TransparentPaper(new Coordinate[] {coordinate, coordinate2});
        // When
        transparentPaper.foldOnX(3);
        // Then
        assertThat(transparentPaper.getDots().size(), is(2));
        assertThat(transparentPaper.getDots(), hasItem(coordinate));
        assertThat(transparentPaper.getDots(), hasItem(new Coordinate(1, 0)));
      }
    }

    @Nested
    @DisplayName("On Y")
    class OnY {

      @Test
      @DisplayName("Should throw exception when can't fold on Y dimension")
      void shouldThrowExceptionWhenCantFoldOnYDimension() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        TransparentPaper transparentPaper = new TransparentPaper(new Coordinate[] {coordinate});
        // When
        AdventOfCodeException exception =
            assertThrows(
                AdventOfCodeException.class,
                () -> {
                  transparentPaper.foldOnY(10);
                });
        // Then
        assertThat(exception.getMessage(), is("Fold exceeds papers height"));
      }

      @Test
      @DisplayName("Should fold and merge coordinates when folding")
      void shouldFoldAndMergeCoordinatesWhenFolding() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        Coordinate coordinate2 = new Coordinate(0, 2);
        TransparentPaper transparentPaper =
            new TransparentPaper(new Coordinate[] {coordinate, coordinate2});
        // When
        transparentPaper.foldOnY(1);
        // Then
        assertThat(transparentPaper.getDots().size(), is(1));
        assertThat(transparentPaper.getDots(), hasItem(coordinate));
      }

      @Test
      @DisplayName("Should fold and merge coordinates when folding on higher paper")
      void shouldFoldAndMergeCoordinatesWhenFoldingOnHigherPaper() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        Coordinate coordinate2 = new Coordinate(0, 6);
        TransparentPaper transparentPaper =
            new TransparentPaper(new Coordinate[] {coordinate, coordinate2});
        // When
        transparentPaper.foldOnY(3);
        // Then
        assertThat(transparentPaper.getDots().size(), is(1));
        assertThat(transparentPaper.getDots(), hasItem(coordinate));
      }

      @Test
      @DisplayName(
          "Should fold and merge coordinates when folding on wider paper and keep non overlapping dot")
      void shouldFoldAndMergeCoordinatesWhenFoldingOnWiderPaperAndKeepNonOverlappingDot() {
        // Given
        Coordinate coordinate = new Coordinate(0, 0);
        Coordinate coordinate2 = new Coordinate(0, 5);
        TransparentPaper transparentPaper =
            new TransparentPaper(new Coordinate[] {coordinate, coordinate2});
        // When
        transparentPaper.foldOnY(3);
        // Then
        assertThat(transparentPaper.getDots().size(), is(2));
        assertThat(transparentPaper.getDots(), hasItem(coordinate));
        assertThat(transparentPaper.getDots(), hasItem(new Coordinate(0, 1)));
      }
    }
  }
}
