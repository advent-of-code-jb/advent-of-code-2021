package dev.jbriggs.aoc2021.day7;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 7 - Crab submarines tests")
class CrabSubmarinesTest {

  @Nested
  @DisplayName("Need to store positions from String array tests")
  class NeedToStorePositionsFromStringArrayTest{

    @Test
    @DisplayName("Should contain no positions when no input")
    void shouldContainNoPositionsWhenNoInput(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[0]);
      // Then
      assertThat(crabSubmarines.getCrabLocations(), is(empty()));
    }

    @Test
    @DisplayName("Should contain 1 position when 1 input")
    void shouldContainOnePositionsWhenOneInput(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[]{"1"});
      // Then
      assertThat(crabSubmarines.getCrabLocations().size(), is(1));
      assertThat(crabSubmarines.getCrabLocations(), hasItem(1));
    }

    @Test
    @DisplayName("Should contain 5 positions when 5 inputs")
    void shouldContainFivePositionsWhenFiveInputs(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[]{"1","2","3","4","5"});
      // Then
      assertThat(crabSubmarines.getCrabLocations().size(), is(5));
      assertThat(crabSubmarines.getCrabLocations(), hasItem(1));
      assertThat(crabSubmarines.getCrabLocations(), hasItem(2));
      assertThat(crabSubmarines.getCrabLocations(), hasItem(3));
      assertThat(crabSubmarines.getCrabLocations(), hasItem(4));
      assertThat(crabSubmarines.getCrabLocations(), hasItem(5));
    }
  }

  @Nested
  @DisplayName("Need to find most left position tests")
  class NeedToFindMostLeftPositionTests{

    @Test
    @DisplayName("Should return NULL when no positions")
    void shouldReturnNullWhenNoPositions(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[0]);
      // Then
      assertThat(crabSubmarines.getLeftMostPosition(), is(nullValue()));
    }

    @Test
    @DisplayName("Should return 1 when 1 location")
    void shouldReturnOneWhenOnlyOneLocation(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[]{"1"});
      // Then
      assertThat(crabSubmarines.getLeftMostPosition(), is(1));
    }

    @Test
    @DisplayName("Should return 1 when 5 location")
    void shouldReturnOneWhenFiveLocations(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[]{"1", "2", "3", "4", "5"});
      // Then
      assertThat(crabSubmarines.getLeftMostPosition(), is(1));
    }
  }

  @Nested
  @DisplayName("Need to find most right position tests")
  class NeedToFindMostRightPositionTests{

    @Test
    @DisplayName("Should return NULL when no positions")
    void shouldReturnNullWhenNoPositions(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[0]);
      // Then
      assertThat(crabSubmarines.getRightMostPosition(), is(nullValue()));
    }

    @Test
    @DisplayName("Should return 1 when 1 location")
    void shouldReturnOneWhenOnlyOneLocation(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[]{"1"});
      // Then
      assertThat(crabSubmarines.getRightMostPosition(), is(1));
    }

    @Test
    @DisplayName("Should return 5 when 5 location")
    void shouldReturnFiveWhenFiveLocations(){
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      // When
      crabSubmarines.enterCrabLocations(new String[]{"1", "2", "3", "4", "5"});
      // Then
      assertThat(crabSubmarines.getRightMostPosition(), is(5));
    }
  }
}