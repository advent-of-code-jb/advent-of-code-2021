package dev.jbriggs.aoc2021.day7;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 7 tests")
class Day7Test {
  Day7 day;

  @BeforeEach
  void beforeEach() {
    day = new Day7(new PuzzleInputParser());
  }

  @Nested
  @DisplayName("Part one tests")
  class PartOneTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 37 when test data entered")
    void shouldReturn5934WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day7/input.txt");
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("37"));
    }

  }

  @Nested
  @DisplayName("Part two tests")
  class PartTwoTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 37 when test data entered")
    void shouldReturn5934WhenTestDataEntered() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day7/input.txt");
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("168"));
    }

  }
}