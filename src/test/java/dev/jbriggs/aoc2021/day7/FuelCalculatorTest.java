package dev.jbriggs.aoc2021.day7;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 7 - Fuel calculator tests")
class FuelCalculatorTest {

  @Nested
  @DisplayName("Needs to return cheapest fuel")
  class NeedsToReturnCheapestFuelTest {

    @Test
    @DisplayName("Should return 0 when all crabs at location 0")
    void shouldReturn0WhenAllCrabsAtLocation0() {
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(new String[] {"0", "0", "0", "0", "0", "0", "0"});
      // When
      Integer result = FuelCalculator.calculateEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(0));
    }

    @Test
    @DisplayName("Should return 0 when all crabs at location 10")
    void shouldReturn10WhenAllCrabsAtLocation10() {
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(new String[] {"10", "10", "10", "10", "10", "10", "10"});
      // When
      Integer result = FuelCalculator.calculateEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(0));
    }

    @Test
    @DisplayName("Should return 2 when most crabs at location 1 and some at 2 are at 2")
    void shouldReturn1WhenMostCrabsAtLocation1AndSomeAt2() {
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(new String[] {"1", "1", "1", "1", "1", "1", "1", "2", "2"});
      // When
      Integer result = FuelCalculator.calculateEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(2));
    }

    @Test
    @DisplayName("Should return 37 with test input")
    void shouldReturn2WithTestInput() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day7/input.txt");
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(input[0].split(","));
      // When
      Integer result = FuelCalculator.calculateEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(37));
    }
  }

  @Nested
  @DisplayName("Needs to return crab efficient cheapest fuel")
  class NeedsToReturnCrabEfficientTotalFuelTest {

    @Test
    @DisplayName("Should return 0 when all crabs at location 0")
    void shouldReturn0WhenAllCrabsAtLocation0() {
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(new String[] {"0", "0", "0", "0", "0", "0", "0"});
      // When
      Long result = FuelCalculator.calculateCrabEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 0 when all crabs at location 10")
    void shouldReturn10WhenAllCrabsAtLocation10() {
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(new String[] {"10", "10", "10", "10", "10", "10", "10"});
      // When
      Long result = FuelCalculator.calculateCrabEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(0L));
    }

    @Test
    @DisplayName("Should return 2 when most crabs at location 1 and some at 2 are at 2")
    void shouldReturn1WhenMostCrabsAtLocation1AndSomeAt2() {
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(new String[] {"1", "1", "1", "1", "1", "1", "1", "2", "2"});
      // When
      Long result = FuelCalculator.calculateCrabEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(2L));
    }

    @Test
    @DisplayName("Should return 12 when most crabs at location 1 and some at 2 are at 4")
    void shouldReturn4WhenMostCrabsAtLocation1AndSomeAt3() {
      // Given
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(new String[] {"1", "1", "1", "1", "1", "1", "1", "4", "4"});
      // When
      Long result = FuelCalculator.calculateCrabEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(12L));
    }

    @Test
    @DisplayName("Should return 37 with test input")
    void shouldReturn2WithTestInput() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day7/input.txt");
      CrabSubmarines crabSubmarines = new CrabSubmarines();
      crabSubmarines.enterCrabLocations(input[0].split(","));
      // When
      Long result = FuelCalculator.calculateCrabEfficientFuel(crabSubmarines);
      // Then
      assertThat(result, is(168L));
    }
  }
}
