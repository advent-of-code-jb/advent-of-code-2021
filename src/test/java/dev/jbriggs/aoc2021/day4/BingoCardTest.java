package dev.jbriggs.aoc2021.day4;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Day 4 - Bingo card tests")
class BingoCardTest {

  @Test
  @DisplayName("Should load data correctly into card")
  void shouldLoadDataCorrectlyIntoCard() {

    // Given
    String[] input = {
      "22 13 17 11  0", "8  2 23  4 24", "21  9 14 16  7", "6 10  3 18  5", "1 12 20 15 19"
    };

    // When
    BingoCard card = new BingoCard(input);

    // Then
    assertThat(card.getData()[0], is(new Integer[] {22, 13, 17, 11, 0}));
    assertThat(card.getData()[1], is(new Integer[] {8, 2, 23, 4, 24}));
    assertThat(card.getData()[2], is(new Integer[] {21, 9, 14, 16, 7}));
    assertThat(card.getData()[3], is(new Integer[] {6, 10, 3, 18, 5}));
    assertThat(card.getData()[4], is(new Integer[] {1, 12, 20, 15, 19}));
  }

  @Test
  @DisplayName("Should check number on card")
  void shouldCheckNumberOnCard() {
    // Given
    String[] input = {
      "22 13 17 11  0", "8  2 23  4 24", "21  9 14 16  7", "6 10  3 18  5", "1 12 20 15 19"
    };

    // When
    BingoCard card = new BingoCard(input);
    card.checkNumber(22);

    // Then
    assertThat(card.getBingoData(0, 0).isSelected(), is(true));
  }

  @Test
  @DisplayName("Should check number on card and return false if not BINGO")
  void shouldCheckNumberOnCardAndReturnFalseIfNotBingo() {
    // Given
    String[] input = {
      "22 13 17 11  0", "8  2 23  4 24", "21  9 14 16  7", "6 10  3 18  5", "1 12 20 15 19"
    };

    // When
    BingoCard card = new BingoCard(input);
    Boolean bingo = card.checkNumber(22);

    // Then
    assertThat(card.getBingoData(0, 0).isSelected(), is(true));
    assertThat(bingo, is(false));
  }

  @Test
  @DisplayName("Should check numbers on card and return true when BINGO horizontal")
  void shouldCheckNumbersOnCardAndReturnTrueWhenBingoHorizontal() {
    // Given
    String[] input = {
      "22 13 17 11  0", "8  2 23  4 24", "21  9 14 16  7", "6 10  3 18  5", "1 12 20 15 19"
    };

    // When
    BingoCard card = new BingoCard(input);
    Boolean bingo0 = card.checkNumber(22);
    Boolean bingo1 = card.checkNumber(13);
    Boolean bingo2 = card.checkNumber(17);
    Boolean bingo3 = card.checkNumber(11);
    Boolean bingo4 = card.checkNumber(0);

    // Then
    assertThat(card.getBingoData(0, 0).isSelected(), is(true));
    assertThat(card.getBingoData(1, 0).isSelected(), is(true));
    assertThat(card.getBingoData(2, 0).isSelected(), is(true));
    assertThat(card.getBingoData(3, 0).isSelected(), is(true));
    assertThat(card.getBingoData(4, 0).isSelected(), is(true));
    assertThat(bingo0, is(false));
    assertThat(bingo1, is(false));
    assertThat(bingo2, is(false));
    assertThat(bingo3, is(false));
    assertThat(bingo4, is(true));
  }

  @Test
  @DisplayName("Should check numbers on card and return true when BINGO vertical")
  void shouldCheckNumbersOnCardAndReturnTrueWhenBingoVertical() {
    // Given
    String[] input = {
      "22 13 17 11  0", "8  2 23  4 24", "21  9 14 16  7", "6 10  3 18  5", "1 12 20 15 19"
    };

    // When
    BingoCard card = new BingoCard(input);
    Boolean bingo0 = card.checkNumber(22);
    Boolean bingo1 = card.checkNumber(8);
    Boolean bingo2 = card.checkNumber(21);
    Boolean bingo3 = card.checkNumber(6);
    Boolean bingo4 = card.checkNumber(1);

    // Then
    assertThat(card.getBingoData(0, 0).isSelected(), is(true));
    assertThat(card.getBingoData(0, 1).isSelected(), is(true));
    assertThat(card.getBingoData(0, 2).isSelected(), is(true));
    assertThat(card.getBingoData(0, 3).isSelected(), is(true));
    assertThat(card.getBingoData(0, 4).isSelected(), is(true));
    assertThat(bingo0, is(false));
    assertThat(bingo1, is(false));
    assertThat(bingo2, is(false));
    assertThat(bingo3, is(false));
    assertThat(bingo4, is(true));
  }

  @Test
  @DisplayName("Should return total remaining unchecked")
  void shouldReturnTotalRemainingUnchecked() {
    // Given
    String[] input = {"22 13 17 11  0", "1 1 1 1 1", "1 1 1 1 1", "1 1 1 1 1", "1 1 1 1 1"};

    // When
    BingoCard card = new BingoCard(input);
    card.checkNumber(22);
    card.checkNumber(13);
    card.checkNumber(17);
    card.checkNumber(11);
    card.checkNumber(0);

    // Then
    Integer result = card.getTotalRemainingUnchecked();
    assertThat(result, is(20));
  }
}
