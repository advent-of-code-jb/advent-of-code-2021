package dev.jbriggs.aoc2021.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Puzzle Input Parser tests")
class PuzzleInputParserTest {

  PuzzleInputParser puzzleInputParser;

  @BeforeEach
  void beforeAll() {
    puzzleInputParser = new PuzzleInputParser();
  }

  @Nested
  @DisplayName("getPuzzleInputFromFile tests")
  class getPuzzleInputFromFile {

    @Test
    @DisplayName("Should return empty array when file does not exist")
    void shouldReturnEmptyArrayWhenFileDoesNotExist() {
      // Given
      puzzleInputParser = new PuzzleInputParser();
      // When
      String[] result = puzzleInputParser.getPuzzleInputFromFile("NON_EXISTING.TXT");
      // Then
      assertThat(result.length, is(0));
    }

    @Test
    @DisplayName("Should return array of length 10 with text file")
    void shouldReturnArrayOfLength7WithTextFile() {
      // Given
      puzzleInputParser = new PuzzleInputParser();
      // When
      String[] result = puzzleInputParser.getPuzzleInputFromFile("testdata/day1/input.txt");
      // Then
      assertThat(result.length, is(10));
    }
  }

  @Nested
  @DisplayName("getPuzzleInputFromFileInteger tests")
  class getPuzzleInputFromFileInteger {

    @Test
    @DisplayName("Should return empty array when file does not exist")
    void shouldReturnEmptyArrayWhenFileDoesNotExist() {
      // Given
      puzzleInputParser = new PuzzleInputParser();
      // When
      Integer[] result = puzzleInputParser.getPuzzleInputFromFileInteger("NON_EXISTING.TXT");
      // Then
      assertThat(result.length, is(0));
    }

    @Test
    @DisplayName("Should return array of length 10 with text file")
    void shouldReturnArrayOfLength7WithTextFile() {
      // Given
      puzzleInputParser = new PuzzleInputParser();
      // When
      Integer[] result = puzzleInputParser.getPuzzleInputFromFileInteger("testdata/day1/input.txt");
      // Then
      assertThat(result.length, is(10));
    }
  }
}
