package dev.jbriggs.aoc2021.day5;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 5 - Line tests")
class LineTest {

  @Nested
  @DisplayName("Can store two coordinates tests")
  class CanStoreTwoCoordinates {

    @Test
    @DisplayName("Should store two coordinates")
    void shouldStoreTwoCoordinates() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(1, 1);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getCoordinateA(), is(coordinateA));
      assertThat(line.getCoordinateB(), is(coordinateB));
    }
  }

  @Nested
  @DisplayName("Can return line orientation")
  class CanReturnLineOrientation {
    @Test
    @DisplayName("Should return HORIZONTAL when horizontal line")
    void shouldReturnHorizontalWhenHorizontalLine() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(1, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLineOrientation(), is(LineOrientation.HORIZONTAL));
    }

    @Test
    @DisplayName("Should return VERTICAL when vertical line")
    void shouldReturnVerticalWhenVerticalLine() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(0, 1);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLineOrientation(), is(LineOrientation.VERTICAL));
    }

    @Test
    @DisplayName("Should return DIAGONAL when diagonal line")
    void shouldReturnDiagonalWhenDiagonalLine() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(1, 1);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLineOrientation(), is(LineOrientation.DIAGONAL));
    }
  }

  @Nested
  @DisplayName("Can return line of coordinates between points")
  class CanReturnListOfCoordinatesInLine {

    @Test
    @DisplayName("Should return list with 2 coordinates")
    void shouldReturnListWithTwoCoordinates() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(0, 1);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(2));
      assertThat(line.getLine(), is(Arrays.asList(coordinateA, coordinateB)));
    }

    @Test
    @DisplayName("Should return horizontal list with 3 coordinates")
    void shouldReturnHorizontalListWithThreeCoordinates() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(2, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(3));
      assertThat(line.getLine(), is(Arrays.asList(coordinateA, new Coordinate(1, 0), coordinateB)));
    }

    @Test
    @DisplayName("Should return vertical list with 3 coordinates")
    void shouldReturnVerticalListWithThreeCoordinates() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(0, 2);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(3));
      assertThat(line.getLine(), is(Arrays.asList(coordinateA, new Coordinate(0, 1), coordinateB)));
    }

    @Test
    @DisplayName("Should return diagonal list with 3 coordinates")
    void shouldReturnDiagonalListWithThreeCoordinates() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(2, 2);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(3));
      assertThat(line.getLine(), is(Arrays.asList(coordinateA, new Coordinate(1, 1), coordinateB)));
    }

    @Test
    @DisplayName("Should return diagonal list with 3 coordinates 2")
    void shouldReturnDiagonalListWithThreeCoordinates2() {
      // Given
      Coordinate coordinateA = new Coordinate(2, 0);
      Coordinate coordinateB = new Coordinate(0, 2);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(3));
      assertThat(line.getLine(), is(Arrays.asList(coordinateA, new Coordinate(1, 1), coordinateB)));
    }

    @Test
    @DisplayName("Should return diagonal list with 3 coordinates 3")
    void shouldReturnDiagonalListWithThreeCoordinates3() {
      // Given
      Coordinate coordinateA = new Coordinate(2, 2);
      Coordinate coordinateB = new Coordinate(0, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(3));
      assertThat(line.getLine(), is(Arrays.asList(coordinateA, new Coordinate(1, 1), coordinateB)));
    }

    @Test
    @DisplayName("Should return diagonal list with 3 coordinates 4")
    void shouldReturnDiagonalListWithThreeCoordinates4() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 2);
      Coordinate coordinateB = new Coordinate(2, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(3));
      assertThat(line.getLine(), is(Arrays.asList(coordinateA, new Coordinate(1, 1), coordinateB)));
    }

    @Test
    @DisplayName("Should return horizontal list with multiple coordinates")
    void shouldReturnHorizontalListWithMultipleCoordinates() {
      // Given
      Coordinate coordinateA = new Coordinate(9, 4);
      Coordinate coordinateB = new Coordinate(3, 4);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getLine().size(), is(7));
      assertThat(
          line.getLine(),
          is(
              Arrays.asList(
                  coordinateA,
                  new Coordinate(8, 4),
                  new Coordinate(7, 4),
                  new Coordinate(6, 4),
                  new Coordinate(5, 4),
                  new Coordinate(4, 4),
                  coordinateB)));
    }
  }

  @Nested
  @DisplayName("Should be able to get max X and Y")
  class MaxXAndY {

    @Test
    @DisplayName("Should return 5 for max X")
    void shouldReturn5ForMaxX() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(5, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMaxX(), is(5));
    }

    @Test
    @DisplayName("Should return 5 for max X inverted")
    void shouldReturn5ForMaxXInverted() {
      // Given
      Coordinate coordinateA = new Coordinate(5, 0);
      Coordinate coordinateB = new Coordinate(0, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMaxX(), is(5));
    }

    @Test
    @DisplayName("Should return 5 for max Y")
    void shouldReturn5ForMaxY() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 0);
      Coordinate coordinateB = new Coordinate(0, 5);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMaxY(), is(5));
    }

    @Test
    @DisplayName("Should return 5 for max Y inverted")
    void shouldReturn5ForMaxYInverted() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 5);
      Coordinate coordinateB = new Coordinate(0, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMaxY(), is(5));
    }
  }

  @Nested
  @DisplayName("Should be able to get min X and Y")
  class MinXAndY {

    @Test
    @DisplayName("Should return 1 for min X")
    void shouldReturn1ForMinX() {
      // Given
      Coordinate coordinateA = new Coordinate(1, 0);
      Coordinate coordinateB = new Coordinate(5, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMinX(), is(1));
    }

    @Test
    @DisplayName("Should return 1 for min X inverted")
    void shouldReturn5ForMinXInverted() {
      // Given
      Coordinate coordinateA = new Coordinate(5, 0);
      Coordinate coordinateB = new Coordinate(1, 0);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMinX(), is(1));
    }

    @Test
    @DisplayName("Should return 1 for min Y")
    void shouldReturn1ForMinY() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 1);
      Coordinate coordinateB = new Coordinate(0, 5);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMinY(), is(1));
    }

    @Test
    @DisplayName("Should return 1 for min Y inverted")
    void shouldReturn1ForMinYInverted() {
      // Given
      Coordinate coordinateA = new Coordinate(0, 5);
      Coordinate coordinateB = new Coordinate(0, 1);
      // When
      Line line = new Line(coordinateA, coordinateB);
      // Then
      assertThat(line.getMinY(), is(1));
    }
  }

  @Nested
  @DisplayName("Should be able to return if line intersects or not")
  class DoesLineIntersect {

    @Test
    @DisplayName("Should return FALSE if lines do not intersect")
    void shouldReturnFalseIfLinesDoNotIntersect() {
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 0));
      Line paramLine = new Line(new Coordinate(0, 1), new Coordinate(5, 1));
      // When
      Boolean result = line.lineIntersects(paramLine);
      // Then
      assertThat(result, is(false));
    }

    @Test
    @DisplayName("Should return TRUE if line contains coordinates from param line")
    void shouldReturnTrueIfVerticalLineIntersectsWithHorizontalLine() {
      /*
        |A
        |
       -|- - - -B
        |
      */
      // Given
      Line line = new Line(new Coordinate(0, 2), new Coordinate(5, 2));
      Line paramLine = new Line(new Coordinate(2, 0), new Coordinate(2, 5));
      // When
      Boolean result = line.lineIntersects(paramLine);
      // Then
      assertThat(result, is(true));
    }
  }

  @Nested
  @DisplayName("Should be able to return coordinates in intersection")
  class ReturnCoordinatesInIntersection {

    @Test
    @DisplayName("Should return empty list if lines do not intersect")
    void shouldReturnEmptyListIfLinesDoNotIntersect() {
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 0));
      Line paramLine = new Line(new Coordinate(0, 1), new Coordinate(5, 1));
      // When
      List<Coordinate> result = line.lineIntersectionPoints(paramLine);
      // Then
      assertThat(result, is(empty()));
    }

    @Test
    @DisplayName(
        "Should return list with one coordinate if line contains coordinates from param line")
    void shouldReturnListWith1CoordinateIfLineContainsCoordinatesFromParamLine() {
      /*
        |A
        |
       -|- - - -B
        |
      */
      // Given
      Line line = new Line(new Coordinate(0, 2), new Coordinate(5, 2));
      Line paramLine = new Line(new Coordinate(2, 0), new Coordinate(2, 5));
      // When
      List<Coordinate> result = line.lineIntersectionPoints(paramLine);
      // Then
      assertThat(result.size(), is(1));
      assertThat(result.get(0), is(new Coordinate(2, 2)));
    }

    @Test
    @DisplayName(
        "Should return list with 4 horizontal coordinates if line contains coordinates from param line")
    void shouldReturnListWith4CoordinateIfLineContainsCoordinatesFromParamLine() {
      // Given
      // Two overlapping horizontal lines
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 0));
      Line paramLine = new Line(new Coordinate(2, 0), new Coordinate(6, 0));
      // When
      List<Coordinate> result = line.lineIntersectionPoints(paramLine);
      // Then
      assertThat(result.size(), is(4));
      assertThat(result.get(0), is(new Coordinate(2, 0)));
      assertThat(result.get(1), is(new Coordinate(3, 0)));
      assertThat(result.get(2), is(new Coordinate(4, 0)));
      assertThat(result.get(3), is(new Coordinate(5, 0)));
    }

    @Test
    @DisplayName(
        "Should return list with 4 vertical coordinates if line contains coordinates from param line")
    void shouldReturnListWith4VerticalCoordinateIfLineContainsCoordinatesFromParamLine() {
      // Given
      // Two overlapping horizontal lines
      Line line = new Line(new Coordinate(0, 0), new Coordinate(0, 5));
      Line paramLine = new Line(new Coordinate(0, 2), new Coordinate(0, 6));
      // When
      List<Coordinate> result = line.lineIntersectionPoints(paramLine);
      // Then
      assertThat(result.size(), is(4));
      assertThat(result.get(0), is(new Coordinate(0, 2)));
      assertThat(result.get(1), is(new Coordinate(0, 3)));
      assertThat(result.get(2), is(new Coordinate(0, 4)));
      assertThat(result.get(3), is(new Coordinate(0, 5)));
    }
  }
}
