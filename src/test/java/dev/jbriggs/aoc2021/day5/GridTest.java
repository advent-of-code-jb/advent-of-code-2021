package dev.jbriggs.aoc2021.day5;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 5 - Grid tests")
class GridTest {

  Grid grid = new Grid();

  @Nested
  @DisplayName("Storing lines (excludes diagonal)")
  class StoringLines{

    @Test
    @DisplayName("Should return empty list when no lines added")
    void shouldReturnEmptyListWhenNoLinesAdded(){
      // Given
      // No lines added
      // When
      List<Line> result = grid.getLines();
      // Then
      assertThat(result, is(empty()));
    }

    @Test
    @DisplayName("Should return 1 line when line added")
    void shouldReturn1LineWhenLineAdded(){
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 0));
      // When
      grid.addLine(line);
      List<Line> result = grid.getLines();
      // Then
      assertThat(result.size(), is(1));
      assertThat(result.get(0), is(line));
    }

    @Test
    @DisplayName("Should return 0 lines when diagonal line attempted added")
    void shouldReturn0LinesWhenDiagonalLineAttempted(){
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 5));
      // When
      grid.addLine(line);
      List<Line> result = grid.getLines();
      // Then
      assertThat(result, is(empty()));
    }

    @Test
    @DisplayName("Should return 1 lines when diagonal line added and flag passed")
    void shouldReturn1LinesWhenDiagonalLineAndFlagPassed(){
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 5));
      // When
      grid.addLine(line, true);
      List<Line> result = grid.getLines();
      // Then
      assertThat(result.size(), is(1));
      assertThat(result.get(0), is(line));
    }

  }

  @Nested
  @DisplayName("Returning max X and Y coordinate")
  class MaxXAndY{

    @Test
    @DisplayName("Should return max 0 for both x and y when line doesn't exist")
    void shouldReturnMax0ForBothXAndYWhenNoLinesExist(){
      // Given
      // No line
      // When
      Coordinate result = grid.getMaxCoordinate();
      // Then
      assertThat(result.getX(), is(0));
      assertThat(result.getY(), is(0));
    }

    @Test
    @DisplayName("Should return max 5,0 line 0,0 -> 5,0 exists")
    void shouldReturnMaxX(){
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 0));
      // When
      grid.addLine(line);
      Coordinate result = grid.getMaxCoordinate();
      // Then
      assertThat(result.getX(), is(5));
      assertThat(result.getY(), is(0));
    }

    @Test
    @DisplayName("Should return max 0,5 line 0,0 -> 0,5 exists")
    void shouldReturnMaxY(){
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(0, 5));
      // When
      grid.addLine(line);
      Coordinate result = grid.getMaxCoordinate();
      // Then
      assertThat(result.getX(), is(0));
      assertThat(result.getY(), is(5));
    }
  }

  @Nested
  @DisplayName("Returning min X and Y coordinate")
  class MinXAndY{

    @Test
    @DisplayName("Should return min 0 for both x and y when line doesn't exist")
    void shouldReturnMin0ForBothXAndYWhenNoLinesExist(){
      // Given
      // No line
      // When
      Coordinate result = grid.getMinCoordinate();
      // Then
      assertThat(result.getX(), is(0));
      assertThat(result.getY(), is(0));
    }

    @Test
    @DisplayName("Should return min 5,0 line 0,0 -> 5,0 exists")
    void shouldReturnMinX(){
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(5, 0));
      // When
      grid.addLine(line);
      Coordinate result = grid.getMinCoordinate();
      // Then
      assertThat(result.getX(), is(0));
      assertThat(result.getY(), is(0));
    }

    @Test
    @DisplayName("Should return min 0,5 line 0,0 -> 0,5 exists")
    void shouldReturnMinY(){
      // Given
      Line line = new Line(new Coordinate(0, 0), new Coordinate(0, 5));
      // When
      grid.addLine(line);
      Coordinate result = grid.getMinCoordinate();
      // Then
      assertThat(result.getX(), is(0));
      assertThat(result.getY(), is(0));
    }
  }

  @Nested
  @DisplayName("Needs to return how many points intersect (not including where 3 lines intersect in the same point)")
  class NeedsToReturnHowManyPointsIntersect{

    @Test
    @DisplayName("Should return 0 when no lines")
    void shouldReturn0WhenNoLines(){
      // Given
      // No line
      // When
      Integer result = grid.getTotalIntersectionPoints();
      // Then
      assertThat(result, is(0));
    }

    @Test
    @DisplayName("Should return 0 when 1 line")
    void shouldReturn0When1Line(){
      // Given
      grid.addLine(new Line(new Coordinate(0, 0), new Coordinate(5, 0)));
      // When
      Integer result = grid.getTotalIntersectionPoints();
      // Then
      assertThat(result, is(0));
    }

    @Test
    @DisplayName("Should return 1 when 2 lines intersect at one point")
    void shouldReturn1When2LinesIntersectAtOnePoint(){
      /*
        |A
        |
       -|- - - -B
        |
      */
      // Given
      Line line = new Line(new Coordinate(0, 2), new Coordinate(5, 2));
      Line paramLine = new Line(new Coordinate(2, 0), new Coordinate(2, 5));
      grid.addLine(line);
      grid.addLine(paramLine);
      // When
      Integer result = grid.getTotalIntersectionPoints();
      // Then
      assertThat(result, is(1));
    }

    @Test
    @DisplayName("Should return 1 when 3 lines intersect at one point")
    void shouldReturn1When3LinesIntersectAtOnePoint(){
      /*
        |A
        |
       -|- - - -B
        |C
      */
      // Given
      Line line = new Line(new Coordinate(0, 2), new Coordinate(5, 2));
      Line paramLine = new Line(new Coordinate(0, 2), new Coordinate(0, 5));
      Line paramLine2 = new Line(new Coordinate(0, 0), new Coordinate(0, 2));
      grid.addLine(line);
      grid.addLine(paramLine);
      grid.addLine(paramLine2);
      // When
      Integer result = grid.getTotalIntersectionPoints();
      // Then
      assertThat(result, is(1));
    }

  }

}