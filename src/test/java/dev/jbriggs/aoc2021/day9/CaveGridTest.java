package dev.jbriggs.aoc2021.day9;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 9 - Cave grid tests")
class CaveGridTest {

  @Nested
  @DisplayName("Can store grid of heights")
  class CanStoreGridOfHeights {

    @Test
    @DisplayName("Should store correct number of heights")
    void shouldStoreCorrectNumberOfHeights() {
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345"});
      // When
      List<HeightCoordinate> heightCoordinates = caveGrid.getHeightCoordinates();
      // Then
      assertThat(heightCoordinates.size(), is(5));
    }

    @Test
    @DisplayName("Should store correct number of heights")
    void shouldStoreHeightsWithCorrectHeightValues() {
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345"});
      // When
      List<HeightCoordinate> heightCoordinates = caveGrid.getHeightCoordinates();
      // Then
      assertThat(heightCoordinates.get(0).getHeight(), is(1));
      assertThat(heightCoordinates.get(1).getHeight(), is(2));
      assertThat(heightCoordinates.get(2).getHeight(), is(3));
      assertThat(heightCoordinates.get(3).getHeight(), is(4));
      assertThat(heightCoordinates.get(4).getHeight(), is(5));
    }

    @Test
    @DisplayName("Should store correct coordinate values")
    void shouldStoreCorrectCoordinateValues() {
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345"});
      // When
      List<HeightCoordinate> heightCoordinates = caveGrid.getHeightCoordinates();
      // Then
      assertThat(heightCoordinates.get(0).getX(), is(0));
      assertThat(heightCoordinates.get(0).getY(), is(0));
      assertThat(heightCoordinates.get(1).getX(), is(1));
      assertThat(heightCoordinates.get(1).getY(), is(0));
      assertThat(heightCoordinates.get(2).getX(), is(2));
      assertThat(heightCoordinates.get(2).getY(), is(0));
      assertThat(heightCoordinates.get(3).getX(), is(3));
      assertThat(heightCoordinates.get(3).getY(), is(0));
      assertThat(heightCoordinates.get(4).getX(), is(4));
      assertThat(heightCoordinates.get(4).getY(), is(0));
    }

    @Test
    @DisplayName("Should store correct coordinate values multiline")
    void shouldStoreCorrectCoordinateValuesMultiline() {
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345", "67890"});
      // When
      List<HeightCoordinate> heightCoordinates = caveGrid.getHeightCoordinates();
      // Then
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(0, 1, 1)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(1, 1, 2)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(2, 1, 3)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(3, 1, 4)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(4, 1, 5)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(0, 0, 6)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(1, 0, 7)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(2, 0, 8)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(3, 0, 9)));
      assertThat(heightCoordinates, hasItem(new HeightCoordinate(4, 0, 0)));
    }
  }

  @Nested
  @DisplayName("Can return map width/ height")
  class CanReturnMaxXYCoordinatesTests{

    @Test
    @DisplayName("Should return map width")
    void shouldReturnMapWidth(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345", "67890"});
      // When
      int width = caveGrid.getGridWidth();
      // Then
      assertThat(width, is(5));
    }

    @Test
    @DisplayName("Should return map height")
    void shouldReturnMapHeight(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345", "67890"});
      // When
      int width = caveGrid.getGridHeight();
      // Then
      assertThat(width, is(2));
    }
  }

  @Nested
  @DisplayName("Needs to return specific coordinate given x and y")
  class CanReturnSpecificCoordinateGivenXAndYTest{

    @Test
    @DisplayName("Should return empty optional if coordinate does not exist")
    void shouldReturnEmptyOptionalIfCoordinateDoesNotExist(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345", "67890"});
      // When
      Optional<HeightCoordinate> coordinateOptional = caveGrid.getHeightCoordinate(10, 10);
      // Then
      assertThat(coordinateOptional.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Should return coordinate if coordinate does exist")
    void shouldReturnCoordinateIfCoordinateDoesExist(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"12345", "67890"});
      // When
      Optional<HeightCoordinate> coordinateOptional = caveGrid.getHeightCoordinate(1, 0);
      // Then
      assertThat(coordinateOptional.isPresent(), is(true));
      assertThat(coordinateOptional.get().getX(), is(1));
      assertThat(coordinateOptional.get().getY(), is(0));
      assertThat(coordinateOptional.get().getHeight(), is(7));
    }
  }

  @Nested
  @DisplayName("Can return neighbours of coordinate")
  class CanReturnNeighboursOfCoordinate{

    @Test
    @DisplayName("Should return neighbours of coordinate")
    void shouldReturnNeighboursOfCoordinate(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"39878", "98567", "87678"});
      // When
      Optional<HeightCoordinate> coordinateOptional = caveGrid.getHeightCoordinate(1, 1);
      assertThat(coordinateOptional.isPresent(), is(true));
      List<HeightCoordinate> neighbours = caveGrid.getNeighbours(coordinateOptional.get());
      // Then
      assertThat(coordinateOptional.get().getX(), is(1));
      assertThat(coordinateOptional.get().getY(), is(1));
      assertThat(coordinateOptional.get().getHeight(), is(8));
      assertThat(neighbours.size(), is(4));
      assertThat(neighbours, hasItem(new HeightCoordinate(1, 2, 9)));
      assertThat(neighbours, hasItem(new HeightCoordinate(0, 1, 9)));
      assertThat(neighbours, hasItem(new HeightCoordinate(2, 1, 5)));
      assertThat(neighbours, hasItem(new HeightCoordinate(1, 0, 7)));
    }
  }

  @Nested
  @DisplayName("Can return if coordinate is low point")
  class CanReturnIfCoordinateIsLowPoint{

    @Test
    @DisplayName("Should return FALSE if coordinate is not low point")
    void shouldReturnFalseIfCoordinateIsNotLowPoint(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"39878", "98567", "87678"});
      // When
      Optional<HeightCoordinate> coordinateOptional = caveGrid.getHeightCoordinate(1, 1);
      assertThat(coordinateOptional.isPresent(), is(true));
      Boolean isLowPoint = caveGrid.isCoordinateLowPoint(coordinateOptional.get());
      // Then
      assertThat(coordinateOptional.get().getX(), is(1));
      assertThat(coordinateOptional.get().getY(), is(1));
      assertThat(coordinateOptional.get().getHeight(), is(8));
      assertThat(isLowPoint, is(false));
    }

    @Test
    @DisplayName("Should return TRUE if coordinate is low point")
    void shouldReturnTrueIfCoordinateIsLowPoint(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"39878", "98567", "87678"});
      // When
      Optional<HeightCoordinate> coordinateOptional = caveGrid.getHeightCoordinate(2, 1);
      assertThat(coordinateOptional.isPresent(), is(true));
      Boolean isLowPoint = caveGrid.isCoordinateLowPoint(coordinateOptional.get());
      // Then
      assertThat(coordinateOptional.get().getX(), is(2));
      assertThat(coordinateOptional.get().getY(), is(1));
      assertThat(coordinateOptional.get().getHeight(), is(5));
      assertThat(isLowPoint, is(true));
    }

    @Test
    @DisplayName("Should return TRUE if coordinate is low point and on edge")
    void shouldReturnTrueIfCoordinateIsLowPointAndOnEdge(){
      // Given
      CaveGrid caveGrid = new CaveGrid(new String[] {"98567", "87678"});
      // When
      Optional<HeightCoordinate> coordinateOptional = caveGrid.getHeightCoordinate(2, 1);
      assertThat(coordinateOptional.isPresent(), is(true));
      Boolean isLowPoint = caveGrid.isCoordinateLowPoint(coordinateOptional.get());
      // Then
      assertThat(coordinateOptional.get().getX(), is(2));
      assertThat(coordinateOptional.get().getY(), is(1));
      assertThat(coordinateOptional.get().getHeight(), is(5));
      assertThat(isLowPoint, is(true));
    }
  }

  @Nested
  @DisplayName("Needs to return list of low points")
  class NeedsToReturnListOfLowPoints{

    @Test
    @DisplayName("Should return 4 results when using test data")
    void shouldReturn4ResultsWhenUsingTestData(){
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day9/input.txt");
      CaveGrid caveGrid = new CaveGrid(input);
      // When
      List<HeightCoordinate> result = caveGrid.getLowPoints();
      // Then
      assertThat(result.size(), is(4));
      assertThat(result, hasItem(new HeightCoordinate(2, 2, 5)));
      assertThat(result, hasItem(new HeightCoordinate(6, 0, 5)));
      assertThat(result, hasItem(new HeightCoordinate(1, 4, 1)));
      assertThat(result, hasItem(new HeightCoordinate(9, 4, 0)));
    }

  }

  @Nested
  @DisplayName("Needs to return list of basins")
  class NeedsToReturnListOfBasins{

    @Test
    @DisplayName("Should return 4 results when using test data")
    void shouldReturn4ResultsWhenUsingTestData(){
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day9/input.txt");
      CaveGrid caveGrid = new CaveGrid(input);
      // When
      List<Basin> result = caveGrid.getBasins();
      // Then
      assertThat(result.size(), is(4));
      assertThat(result.get(0).getCoordinates(), hasItem(new HeightCoordinate(1, 4, 1)));
      assertThat(result.get(1).getCoordinates(), hasItem(new HeightCoordinate(2, 2, 5)));
      assertThat(result.get(2).getCoordinates(), hasItem(new HeightCoordinate(6, 0, 5)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(9, 4, 0)));
    }

    @Test
    @DisplayName("Should return basin with 3 coordinates")
    void shouldReturnBasinWith3Coordinates(){
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day9/input.txt");
      CaveGrid caveGrid = new CaveGrid(input);
      // When
      List<Basin> result = caveGrid.getBasins();
      // Then
      assertThat(result.size(), is(4));
      assertThat(result.get(0).getCoordinates().size(), is(3));
      assertThat(result.get(0).getCoordinates(), hasItem(new HeightCoordinate(1, 4, 1)));
      assertThat(result.get(0).getCoordinates(), hasItem(new HeightCoordinate(0, 4, 2)));
      assertThat(result.get(0).getCoordinates(), hasItem(new HeightCoordinate(0, 3, 3)));
    }

    @Test
    @DisplayName("Should return basin with 9 coordinates")
    void shouldReturnBasinWith9Coordinates(){
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day9/input.txt");
      CaveGrid caveGrid = new CaveGrid(input);
      // When
      List<Basin> result = caveGrid.getBasins();
      // Then
      assertThat(result.size(), is(4));
      assertThat(result.get(3).getCoordinates().size(), is(9));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(9, 4, 0)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(8, 4, 1)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(7, 4, 2)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(6, 4, 3)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(5, 4, 4)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(6, 3, 4)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(8, 3, 2)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(9, 3, 1)));
      assertThat(result.get(3).getCoordinates(), hasItem(new HeightCoordinate(9, 2, 2)));
    }
  }


}
