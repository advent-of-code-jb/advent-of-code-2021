package dev.jbriggs.aoc2021.day9;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 9 - Basin tests")
class BasinTest {

  @Nested
  @DisplayName("Needs to store list of coordinates")
  class NeedsToStoreListOfCoordinatesTest {

    @Test
    @DisplayName("Should store coordinates in basin")
    void shouldStoreCoordinatesInBasin() {
      // Given
      HeightCoordinate heightCoordinate1 = new HeightCoordinate(0, 0, 2);
      HeightCoordinate heightCoordinate2 = new HeightCoordinate(1, 0, 3);
      Basin basin = new Basin(heightCoordinate1);
      // When
      basin.addCoordinate(heightCoordinate2);
      // Then
      assertThat(basin.getCoordinates(), hasItem(heightCoordinate1));
      assertThat(basin.getCoordinates(), hasItem(heightCoordinate2));
    }
  }

  @Nested
  @DisplayName("Needs to return size of basin")
  class NeedsToReturnSizeOfBasinTest{

    @Test
    @DisplayName("Should return size of basin")
    void shouldReturnSizeOfBasin(){
      // Given
      HeightCoordinate heightCoordinate1 = new HeightCoordinate(0, 0, 2);
      HeightCoordinate heightCoordinate2 = new HeightCoordinate(1, 0, 3);
      Basin basin = new Basin(heightCoordinate1);
      // When
      basin.addCoordinate(heightCoordinate2);
      // Then
      assertThat(basin.getSize(), is(2));
    }

  }

  @Nested
  @DisplayName("Needs to return boolean if coordinate exists in basin")
  class NeedsToReturnBooleanIfCoordinateExistsInBasin{

    @Test
    @DisplayName("Should return FALSE if coordinate does not exist in basin")
    void shouldReturnFalseIfCoordinateDoesNotExistInBasin(){
      // Given
      HeightCoordinate heightCoordinate1 = new HeightCoordinate(0, 0, 2);
      HeightCoordinate heightCoordinate2 = new HeightCoordinate(1, 0, 3);
      Basin basin = new Basin(heightCoordinate1);
      basin.addCoordinate(heightCoordinate2);
      // When
      boolean result = basin.doesCoordinateExist(new HeightCoordinate(10, 10, 1));
      // Then
      assertThat(result, is(false));
    }

    @Test
    @DisplayName("Should return TRUE if coordinate does exist in basin")
    void shouldReturnTrueIfCoordinateDoesExistInBasin(){
      // Given
      HeightCoordinate heightCoordinate1 = new HeightCoordinate(0, 0, 2);
      Basin basin = new Basin(heightCoordinate1);
      // When
      boolean result = basin.doesCoordinateExist(heightCoordinate1);
      // Then
      assertThat(result, is(true));
    }
  }
}
