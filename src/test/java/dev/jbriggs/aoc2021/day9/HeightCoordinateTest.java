package dev.jbriggs.aoc2021.day9;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 9 - Height coordinate tests")
class HeightCoordinateTest {

  @Nested
  @DisplayName("Can store height tests")
  class CanStoreHeightTest{

    @Test
    @DisplayName("Should return height")
    void shouldReturnHeight(){
      // Given
      HeightCoordinate heightCoordinate = new HeightCoordinate(0, 0, 5);
      // Then
      assertThat(heightCoordinate.getHeight(), is(5));
    }
  }

  @Nested
  @DisplayName("Can return risk tests")
  class CanReturnRiskTest{

    @Test
    @DisplayName("Should return height + 1 for risk")
    void shouldReturnRisk(){
      // Given
      HeightCoordinate heightCoordinate = new HeightCoordinate(0, 0, 5);
      // Then
      assertThat(heightCoordinate.getRiskLevel(), is(6));
    }
  }
}