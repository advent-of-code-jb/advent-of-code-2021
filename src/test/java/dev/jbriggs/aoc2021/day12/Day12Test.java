package dev.jbriggs.aoc2021.day12;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 12 tests")
class Day12Test {
  Day12 day;

  @BeforeEach
  void beforeEach() {
    day = new Day12(new PuzzleInputParser());
  }

  @Nested
  @DisplayName("Part one tests")
  class PartOneTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 226 when using test input")
    void shouldReturn226WhenUsingTestInput() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day12/input.txt");
      // When
      String result = day.partOne(input);
      // Then
      assertThat(result, is("226"));
    }

  }

  @Nested
  @DisplayName("Part two tests")
  class PartTwoTests{

    @Test
    @DisplayName("Should return 0 when no input")
    void shouldReturn0WhenNoInput() {
      // Given
      String[] input = new String[0];
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("0"));
    }

    @Test
    @DisplayName("Should return 3509 when using test input")
    void shouldReturn3509WhenUsingTestInput() {
      // Given
      String[] input = new PuzzleInputParser().getPuzzleInputFromFile("testdata/day12/input.txt");
      // When
      String result = day.partTwo(input);
      // Then
      assertThat(result, is("3509"));
    }

  }
}