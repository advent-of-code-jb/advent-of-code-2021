package dev.jbriggs.aoc2021.day12;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Day 12 - Cave system tests")
class CaveSystemTest {

  @Nested
  @DisplayName("Should be able to store paths in cave system")
  class shouldBeABeAbleToStorePathsInCaveSystem {

    @Test
    @DisplayName("Should store two paths when two caves entered")
    void canStoreTwoPaths() {
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave caveA = new Cave("A");
      Cave caveB = new Cave("B");
      // When
      caveSystem.addPath(caveA, caveB);
      // Then
      assertThat(caveSystem.getPaths(), hasItem(new CavePath(caveA, caveB)));
      assertThat(caveSystem.getPaths(), hasItem(new CavePath(caveB, caveA)));
    }

    @Test
    @DisplayName("Should store one path when two caves entered and one is 'start'")
    void shouldOnlyStoreOnePathWhenStartCavePassed() {
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave start = new Cave("start");
      Cave caveB = new Cave("B");
      // When
      caveSystem.addPath(start, caveB);
      // Then
      assertThat(caveSystem.getPaths().size(), is(1));
      assertThat(caveSystem.getPaths(), hasItem(new CavePath(start, caveB)));
    }

    @Test
    @DisplayName("Should store one path when two caves entered and one is 'start' invert")
    void shouldOnlyStoreOnePathWhenStartCavePassedInvert() {
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave start = new Cave("start");
      Cave caveB = new Cave("B");
      // When
      caveSystem.addPath(caveB, start);
      // Then
      assertThat(caveSystem.getPaths().size(), is(1));
      assertThat(caveSystem.getPaths(), hasItem(new CavePath(start, caveB)));
    }

    @Test
    @DisplayName("Should store one path when two caves entered and one is 'end'")
    void shouldOnlyStoreOnePathWhenendCavePassed() {
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave end = new Cave("end");
      Cave caveB = new Cave("B");
      // When
      caveSystem.addPath(end, caveB);
      // Then
      assertThat(caveSystem.getPaths().size(), is(1));
      assertThat(caveSystem.getPaths(), hasItem(new CavePath(caveB, end)));
    }

    @Test
    @DisplayName("Should store one path when two caves entered and one is 'end' invert")
    void shouldOnlyStoreOnePathWhenendCavePassedInvert() {
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave end = new Cave("end");
      Cave caveB = new Cave("B");
      // When
      caveSystem.addPath(caveB, end);
      // Then
      assertThat(caveSystem.getPaths().size(), is(1));
      assertThat(caveSystem.getPaths(), hasItem(new CavePath(caveB, end)));
    }
  }

  @Nested
  @DisplayName("Should be able to store all possible routes from start to end")
  class shouldBeAbleToReturnAllPossibleRoutesFromStartToEnd{

    @Test
    @DisplayName("Should return one route start - end")
    void shouldReturnOneRouteWhenJustStartend(){
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave start = new Cave("start");
      Cave end = new Cave("end");
      caveSystem.addPath(start,end);
      // When
      List<String> result = caveSystem.getAllPossiblePaths();
      // Then
      assertThat(result.size(), is(1));
      assertThat(result.get(0), is("start,end"));
    }

    @Test
    @DisplayName("Should return one route start A end")
     void shouldReturnOneRouteWhenJustStartAend(){
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave start = new Cave("start");
      Cave caveA = new Cave("A");
      Cave end = new Cave("end");
      caveSystem.addPath(start,caveA);
      caveSystem.addPath(caveA,end);
      // When
      List<String> result = caveSystem.getAllPossiblePaths();
      // Then
      assertThat(result.size(), is(1));
      assertThat(result.get(0), is("start,A,end"));
    }

    @Test
    @DisplayName("Should return one route start A B end")
    void shouldReturnOneRouteWhenJustStartABend(){
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave start = new Cave("start");
      Cave caveA = new Cave("A");
      Cave caveB = new Cave("b");
      Cave end = new Cave("end");
      caveSystem.addPath(start,caveA);
      caveSystem.addPath(caveA,caveB);
      caveSystem.addPath(caveB,end);
      // When
      List<String> result = caveSystem.getAllPossiblePaths();
      // Then
      assertThat(result.size(), is(1));
      assertThat(result.get(0), is("start,A,b,end"));
    }

    @Test
    @DisplayName("Should return 10 paths from small data set")
    void shouldReturn10PathsFromSmallDataSet(){
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave start = new Cave("start");
      Cave caveA = new Cave("A");
      Cave caveB = new Cave("b");
      Cave caveC = new Cave("c");
      Cave caveD = new Cave("d");
      Cave end = new Cave("end");
      caveSystem.addPath(start,caveA);
      caveSystem.addPath(start,caveB);
      caveSystem.addPath(caveA,caveC);
      caveSystem.addPath(caveA,caveB);
      caveSystem.addPath(caveB,caveD);
      caveSystem.addPath(caveA,end);
      caveSystem.addPath(caveB,end);
      // When
      List<String> result = caveSystem.getAllPossiblePaths();
      // Then
      assertThat(result.size(), is(10));
      assertThat(result, hasItem("start,A,b,A,end"));
      assertThat(result, hasItem("start,A,b,end"));
      assertThat(result, hasItem("start,A,c,A,b,A,end"));
      assertThat(result, hasItem("start,A,c,A,b,end"));
      assertThat(result, hasItem("start,A,c,A,end"));
      assertThat(result, hasItem("start,A,end"));
      assertThat(result, hasItem("start,b,A,c,A,end"));
      assertThat(result, hasItem("start,b,A,end"));
      assertThat(result, hasItem("start,b,end"));
    }

    @Test
    @DisplayName("Should return 36 paths from small data set")
    void shouldReturn36PathsFromSmallDataSet(){
      // Given
      CaveSystem caveSystem = new CaveSystem();
      Cave start = new Cave("start");
      Cave caveA = new Cave("A");
      Cave caveB = new Cave("b");
      Cave caveC = new Cave("c");
      Cave caveD = new Cave("d");
      Cave end = new Cave("end");
      caveSystem.addPath(start,caveA);
      caveSystem.addPath(start,caveB);
      caveSystem.addPath(caveA,caveC);
      caveSystem.addPath(caveA,caveB);
      caveSystem.addPath(caveB,caveD);
      caveSystem.addPath(caveA,end);
      caveSystem.addPath(caveB,end);
      // When
      List<String> result = caveSystem.getAllPossiblePaths(2);
      // Then
      assertThat(result.size(), is(36));
      assertThat(result, hasItem("start,A,b,A,b,A,c,A,end"));
      assertThat(result, hasItem("start,A,b,A,b,A,end"));
      assertThat(result, hasItem("start,A,b,A,b,end"));
      assertThat(result, hasItem("start,A,b,A,c,A,b,A,end"));
      assertThat(result, hasItem("start,A,b,A,c,A,b,end"));
      assertThat(result, hasItem("start,A,b,A,c,A,c,A,end"));
      assertThat(result, hasItem("start,A,b,A,c,A,end"));
      assertThat(result, hasItem("start,A,b,A,end"));
      assertThat(result, hasItem("start,A,b,d,b,A,c,A,end"));
      assertThat(result, hasItem("start,A,b,d,b,A,end"));
      assertThat(result, hasItem("start,A,b,d,b,end"));
      assertThat(result, hasItem("start,A,b,end"));
      assertThat(result, hasItem("start,A,c,A,b,A,b,A,end"));
      assertThat(result, hasItem("start,A,c,A,b,A,b,end"));
      assertThat(result, hasItem("start,A,c,A,b,A,c,A,end"));
      assertThat(result, hasItem("start,A,c,A,b,A,end"));
      assertThat(result, hasItem("start,A,c,A,b,d,b,A,end"));
      assertThat(result, hasItem("start,A,c,A,b,d,b,end"));
      assertThat(result, hasItem("start,A,c,A,b,end"));
      assertThat(result, hasItem("start,A,c,A,c,A,b,A,end"));
      assertThat(result, hasItem("start,A,c,A,c,A,b,end"));
      assertThat(result, hasItem("start,A,c,A,c,A,end"));
      assertThat(result, hasItem("start,A,c,A,end"));
      assertThat(result, hasItem("start,A,end"));
      assertThat(result, hasItem("start,b,A,b,A,c,A,end"));
      assertThat(result, hasItem("start,b,A,b,A,end"));
      assertThat(result, hasItem("start,b,A,b,end"));
      assertThat(result, hasItem("start,b,A,c,A,b,A,end"));
      assertThat(result, hasItem("start,b,A,c,A,b,end"));
      assertThat(result, hasItem("start,b,A,c,A,c,A,end"));
      assertThat(result, hasItem("start,b,A,c,A,end"));
      assertThat(result, hasItem("start,b,A,end"));
      assertThat(result, hasItem("start,b,d,b,A,c,A,end"));
      assertThat(result, hasItem("start,b,d,b,A,end"));
      assertThat(result, hasItem("start,b,d,b,end"));
      assertThat(result, hasItem("start,b,end"));
    }
  }
}
