package dev.jbriggs.aoc2021.day10;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class LineReader {

  private static final Character[] OPEN_BRACKETS = {'(', '[', '{', '<' };
  private static final Character[] CLOSED_BRACKETS = {')', ']', '}', '>' };

  public static long getCorruptedScore(String line) throws AdventOfCodeException{

    int levelsDeep = 0;
    HashMap<Integer, Character> characters = new HashMap<>();
    for(char bracket : line.toCharArray()){
      // Is open bracket, delve one level deep
      if(isOpenBracket(bracket)){
        levelsDeep++;
        characters.put(levelsDeep, bracket);
      }else if(isClosedBracket(bracket)){
        if(!Objects.equals(bracket, getClosingBracket(characters.get(levelsDeep)))){
          return ScoreCalculator.getCorruptedScore(bracket);
        }
        levelsDeep--;
      }else{
        throw new AdventOfCodeException("Illegal character found in row");
      }
    }
    return 0L;
  }

  public static long getIncompleteScore(String line) {

    int levelsDeep = 0;
    HashMap<Integer, Character> characters = new HashMap<>();
    for(char bracket : line.toCharArray()){
      if(isOpenBracket(bracket)){
        levelsDeep++;
        characters.put(levelsDeep, bracket);
      }else if(isClosedBracket(bracket)){
        if(!Objects.equals(bracket, getClosingBracket(characters.get(levelsDeep)))){
          return 0L;
        }
        levelsDeep--;
      }else{
        throw new AdventOfCodeException("Illegal character found in row");
      }
    }

    long result = 0L;
    while(levelsDeep > 0){
      char missingClosingBracket = getClosingBracket(characters.get(levelsDeep));
      result *= 5L;
      result += ScoreCalculator.getIncompleteScore(missingClosingBracket);
      levelsDeep--;
    }
    return result;
  }

  private static Character getClosingBracket(char openBracket){
    for(int i = 0; i < 4; i++){
      if(Objects.equals(OPEN_BRACKETS[i], openBracket)){
        return CLOSED_BRACKETS[i];
      }
    }
    throw new AdventOfCodeException("Bracket not found");
  }

  private static boolean isOpenBracket(char bracket){
    for(char open : OPEN_BRACKETS){
      if(Objects.equals(open, bracket)){
        return true;
      }
    }
    return false;
  }

  private static boolean isClosedBracket(char bracket){
    for(char closed : CLOSED_BRACKETS){
      if(Objects.equals(closed, bracket)){
        return true;
      }
    }
    return false;
  }

  public static long getFullCorruptedScore(String[] lines) {
    long score = 0L;
    for(String line : lines){
      score += getCorruptedScore(line);
    }
    return score;
  }

  public static long getMiddleIncompleteScore(String[] lines) {
    List<Long> results = new ArrayList<>();
    for(String line : lines){
      long incompleteScore = getIncompleteScore(line);
      if(incompleteScore != 0L){
        results.add(incompleteScore);
      }
    }

    if(results.isEmpty()){
      return 0L;
    }


    results.sort(Long::compareTo);
    return results.get((results.size() - 1)/2);
  }
}
