package dev.jbriggs.aoc2021.day10;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import java.util.Objects;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ScoreCalculator {

  public static final long ROUND_BRACKET_CORRUPTED_SCORE = 3L;
  public static final long SQUARE_BRACKET_CORRUPTED_SCORE = 57L;
  public static final long CURLY_BRACKET_CORRUPTED_SCORE = 1197L;
  public static final long ANGLE_BRACKET_CORRUPTED_SCORE = 25137L;

  public static final long ROUND_BRACKET_INCOMPLETE_SCORE = 1L;
  public static final long SQUARE_BRACKET_INCOMPLETE_SCORE = 2L;
  public static final long CURLY_BRACKET_INCOMPLETE_SCORE = 3L;
  public static final long ANGLE_BRACKET_INCOMPLETE_SCORE = 4L;

  public static long getCorruptedScore(char character) throws AdventOfCodeException {
    return getScore(character, true);
  }

  public static long getIncompleteScore(char character) throws AdventOfCodeException{
    return getScore(character, false);

  }

  private static long getScore(char character, boolean corrupted) throws AdventOfCodeException{
    long score;
    if(Objects.equals(character, ')')){
      score = corrupted ? ROUND_BRACKET_CORRUPTED_SCORE : ROUND_BRACKET_INCOMPLETE_SCORE;
    }else if(Objects.equals(character, ']')){
      score = corrupted ? SQUARE_BRACKET_CORRUPTED_SCORE : SQUARE_BRACKET_INCOMPLETE_SCORE;
    }else if(Objects.equals(character, '}')){
      score = corrupted ? CURLY_BRACKET_CORRUPTED_SCORE : CURLY_BRACKET_INCOMPLETE_SCORE;
    }else if(Objects.equals(character, '>')){
      score = corrupted ? ANGLE_BRACKET_CORRUPTED_SCORE : ANGLE_BRACKET_INCOMPLETE_SCORE;
    }else{
      throw new AdventOfCodeException("Invalid character passed into score calculator");
    }
    return score;
  }
}
