package dev.jbriggs.aoc2021.day10;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day10 extends Day<String> {

  public static final int DAY_NUMBER = 10;

  public Day10(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    long result = LineReader.getFullCorruptedScore(input);

    return Long.toString(result);
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    long result = LineReader.getMiddleIncompleteScore(input);

    return Long.toString(result);
  }

  @Override
  public Integer day() {
    return DAY_NUMBER;
  }

  @Override
  public String inputPath() {
    return "inputdata/day10/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
