package dev.jbriggs.aoc2021.day5;

import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.Getter;

@Getter
public class Grid {

  private final List<Line> lines = new ArrayList<>();

  /**
   * Adds line to grid (Excludes diagonal lines)
   *
   * @param line Line to add to grid
   */
  public void addLine(Line line) {
    addLine(line, false);
  }

  /**
   * Adds line to grid
   *
   * @param line Line to add to grid
   */
  public void addLine(Line line, Boolean acceptDiagonal) {
    if (acceptDiagonal || !Objects.equals(line.getLineOrientation(), LineOrientation.DIAGONAL)) {
      lines.add(line);
    }
  }

  public Coordinate getMaxCoordinate() {
    if (lines.isEmpty()) {
      return new Coordinate(0, 0);
    }

    // Max X
    Integer maxX = lines.stream().map(Line::getMaxX).max(Integer::compareTo).orElse(0);
    Integer maxY = lines.stream().map(Line::getMaxY).max(Integer::compareTo).orElse(0);
    return new Coordinate(maxX, maxY);
  }

  public Coordinate getMinCoordinate() {
    if (lines.isEmpty()) {
      return new Coordinate(0, 0);
    }

    // Max X
    Integer maxX = lines.stream().map(Line::getMinX).min(Integer::compareTo).orElse(0);
    Integer maxY = lines.stream().map(Line::getMinY).min(Integer::compareTo).orElse(0);
    return new Coordinate(maxX, maxY);
  }

  /**
   * Intersections which involve 2 or more collisions
   *
   * @return The current number of intersections
   */
  public Integer getTotalIntersectionPoints() {
    return Math.toIntExact(getMap().values().stream().filter(x -> x > 1).count());
  }

  private HashMap<Coordinate, Integer> getMap() {
    HashMap<Coordinate, Integer> map = new HashMap<>();
    Coordinate maxCoordinate = getMaxCoordinate();
    for(int x = 0; x <= maxCoordinate.getX(); x++){
      for(int y = 0; y <= maxCoordinate.getY(); y++){
        map.put(new Coordinate(x, y), 0);
      }
    }

    for(Line line : getLines()){
      for(Coordinate coordinate : line.getLine()){
        map.put(coordinate, map.get(coordinate) + 1);
      }
    }
    return map;
  }

  public String getIntersectionMap() {
    Map<Coordinate, Integer> map = getMap();
    StringBuilder sb = new StringBuilder();
    Coordinate maxCoordinate = getMaxCoordinate();
    sb.append("\n");
    for(int x = 0; x <= maxCoordinate.getX(); x++){
      for(int y = 0; y <= maxCoordinate.getY(); y++){
        sb.append(map.get(new Coordinate(y, x)));
      }
      sb.append("\n");
    }
    return sb.toString();
  }
}
