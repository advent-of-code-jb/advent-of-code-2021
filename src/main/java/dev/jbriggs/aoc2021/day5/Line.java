package dev.jbriggs.aoc2021.day5;

import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Line {

  private final Coordinate coordinateA;
  private final Coordinate coordinateB;

  public List<Coordinate> getLine() {
    List<Coordinate> result = new ArrayList<>();
    // Add A
    result.add(coordinateA);
    // Add in-between
    if (Objects.equals(getLineOrientation(), LineOrientation.HORIZONTAL)) {
      mapHorizontalLine(result);
    }
    if (Objects.equals(getLineOrientation(), LineOrientation.VERTICAL)) {
      mapVerticalLine(result);
    }
    if (Objects.equals(getLineOrientation(), LineOrientation.DIAGONAL)) {
      mapDiagonalLine(result);
    }
    // Add B
    result.add(coordinateB);
    return result;
  }

  private void mapVerticalLine(List<Coordinate> result) {
    if (coordinateA.getY() < coordinateB.getY()) {
      for (int y = coordinateA.getY() + 1; y < coordinateB.getY(); y++) {
        result.add(new Coordinate(coordinateA.getX(), y));
      }
    } else {
      for (int y = coordinateA.getY() - 1; y > coordinateB.getY(); y--) {
        result.add(new Coordinate(coordinateA.getX(), y));
      }
    }
  }

  private void mapDiagonalLine(List<Coordinate> result) {

    List<Integer> xRange = getNumbersInbetween(coordinateA.getX(), coordinateB.getX());
    List<Integer> yRange = getNumbersInbetween(coordinateA.getY(), coordinateB.getY());
    for(int x = 0; x < xRange.size(); x++){
      result.add(new Coordinate(xRange.get(x), yRange.get(x)));
    }
  }

  private List<Integer> getNumbersInbetween(int x, int y){
    List<Integer> result = new ArrayList<>();
    if(x < y){
      for(int i = x + 1; i < y; i++){
        result.add(i);
      }
    }
    if(x > y){
      for(int i = x - 1; i > y; i--){
        result.add(i);
      }
    }
    return result;
  }

  private void mapHorizontalLine(List<Coordinate> result) {
    if (coordinateA.getX() < coordinateB.getX()) {
      for (int x = coordinateA.getX() + 1; x < coordinateB.getX(); x++) {
        result.add(new Coordinate(x, coordinateA.getY()));
      }
    } else {
      for (int x = coordinateA.getX() - 1; x > coordinateB.getX(); x--) {
        result.add(new Coordinate(x, coordinateA.getY()));
      }
    }
  }

  public LineOrientation getLineOrientation() {
    if (Objects.equals(coordinateA.getY(), coordinateB.getY())) {
      return LineOrientation.HORIZONTAL;
    } else if (Objects.equals(coordinateA.getX(), coordinateB.getX())) {
      return LineOrientation.VERTICAL;
    } else {
      return LineOrientation.DIAGONAL;
    }
  }

  @Override
  public String toString() {
    return String.format("%s -> %s", coordinateA, coordinateB);
  }

  public Integer getMaxX() {
    return getLine().stream()
        .max(Comparator.comparing(Coordinate::getX))
        .map(Coordinate::getX)
        .orElse(0);
  }

  public Integer getMaxY() {
    return getLine().stream()
        .max(Comparator.comparing(Coordinate::getY))
        .map(Coordinate::getY)
        .orElse(0);
  }

  public Integer getMinX() {
    return getLine().stream()
        .min(Comparator.comparing(Coordinate::getX))
        .map(Coordinate::getX)
        .orElse(0);
  }

  public Integer getMinY() {
    return getLine().stream()
        .min(Comparator.comparing(Coordinate::getY))
        .map(Coordinate::getY)
        .orElse(0);
  }

  public Boolean lineIntersects(Line paramLine) {
    List<Coordinate> line = this.getLine();
    List<Coordinate> otherLine = paramLine.getLine();
    return line.stream().anyMatch(otherLine::contains);
  }

  public List<Coordinate> lineIntersectionPoints(Line paramLine) {
    List<Coordinate> line = this.getLine();
    List<Coordinate> otherLine = paramLine.getLine();
    return line.stream().filter(otherLine::contains).collect(Collectors.toList());
  }
}
