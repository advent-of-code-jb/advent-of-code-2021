package dev.jbriggs.aoc2021.day5;

public enum LineOrientation {
  HORIZONTAL,
  VERTICAL,
  DIAGONAL
}
