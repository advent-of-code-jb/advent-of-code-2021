package dev.jbriggs.aoc2021.day5;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.common.Coordinate;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day5 extends Day<String> {

  private static final Pattern PATTERN = Pattern.compile("^(\\d+),(\\d+) -> (\\d+),(\\d+)$");

  public Day5(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public boolean skip() {
    return true;
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }
    Grid grid = new Grid();
    for (String current : input) {
      Matcher matcher = PATTERN.matcher(current);
      if (matcher.matches()) {
        grid.addLine(
            new Line(
                new Coordinate(matcher.group(1), matcher.group(2)),
                new Coordinate(matcher.group(3), matcher.group(4))));
      }else{
        throw new AdventOfCodeException("Regex does not match input data");
      }
    }

    log.debug(grid.getIntersectionMap());

    return grid.getTotalIntersectionPoints().toString();
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }
    Grid grid = new Grid();
    for (String current : input) {
      Matcher matcher = PATTERN.matcher(current);
      if (matcher.matches()) {
        grid.addLine(
            new Line(
                new Coordinate(matcher.group(1), matcher.group(2)),
                new Coordinate(matcher.group(3), matcher.group(4))), true);
      }else{
        throw new AdventOfCodeException("Regex does not match input data");
      }
    }

    log.debug(grid.getIntersectionMap());

    return grid.getTotalIntersectionPoints().toString();
  }

  @Override
  public Integer day() {
    return 5;
  }

  @Override
  public String inputPath() {
    return "inputdata/day5/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
