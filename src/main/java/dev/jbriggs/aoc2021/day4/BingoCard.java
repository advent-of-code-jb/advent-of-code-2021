package dev.jbriggs.aoc2021.day4;

import static java.util.Arrays.stream;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("java:S109")
public class BingoCard {

  private static final Pattern pattern =
      Pattern.compile("^(\\d+) +(\\d+) +(\\d+) +(\\d+) +(\\d+)$");

  @Getter @Setter private BingoData[][] bingoData;

  public BingoCard(String[] input) {
    bingoData = new BingoData[5][5];
    int rowCount = 0;

    for (String row : input) {
      Matcher matcher = pattern.matcher(row.replace("  ", " ").trim());
      if (matcher.matches()) {
        bingoData[rowCount][0] = new BingoData(Integer.parseInt(matcher.group(1)));
        bingoData[rowCount][1] = new BingoData(Integer.parseInt(matcher.group(2)));
        bingoData[rowCount][2] = new BingoData(Integer.parseInt(matcher.group(3)));
        bingoData[rowCount][3] = new BingoData(Integer.parseInt(matcher.group(4)));
        bingoData[rowCount][4] = new BingoData(Integer.parseInt(matcher.group(5)));
        rowCount++;
      }
    }
  }

  public Boolean checkNumber(int number) {
    for (BingoData[] row : bingoData) {
      for (BingoData data : row) {
        if (data.number.equals(number)) {
          data.checkNumber();
          return isBingo(bingoData);
        }
      }
    }
    return false;
  }

  private static boolean isBingo(BingoData[][] bingoData) {
    // Check horizontal
    for (BingoData[] row : bingoData) {
      if (stream(row).allMatch(BingoData::isSelected)) {
        return true;
      }
    }
    // Check vertical
    BingoData[][] vertical = new BingoData[5][5];
    for(int x = 0; x < 5; x++){
      for(int y = 0; y < 5; y++){
        vertical[x][y] = bingoData[y][x];
      }
    }
    for (BingoData[] row : vertical) {
      if (stream(row).allMatch(BingoData::isSelected)) {
        return true;
      }
    }
    return false;
  }

  public Integer[][] getData() {
    Integer[][] data = new Integer[5][5];
    for (int y = 0; y < 5; y++) {
      for (int x = 0; x < 5; x++) {
        data[y][x] = bingoData[y][x].number;
      }
    }
    return data;
  }

  public BingoData getBingoData(int x, int y) {
    return bingoData[y][x];
  }

  @Override
  public String toString() {
    String rowFormat = "%d %d %d %d %d";
    return "BingoCard{\n"
        + String.format(
            rowFormat,
            getData()[0][0],
            getData()[0][1],
            getData()[0][2],
            getData()[0][3],
            getData()[0][4])
        + String.format(
            rowFormat,
            getData()[1][0],
            getData()[1][1],
            getData()[1][2],
            getData()[1][3],
            getData()[1][4])
        + String.format(
            rowFormat,
            getData()[2][0],
            getData()[2][1],
            getData()[2][2],
            getData()[2][3],
            getData()[2][4])
        + String.format(
            rowFormat,
            getData()[3][0],
            getData()[3][1],
            getData()[3][2],
            getData()[3][3],
            getData()[3][4])
        + String.format(
            rowFormat,
            getData()[4][0],
            getData()[4][1],
            getData()[4][2],
            getData()[4][3],
            getData()[4][4])
        + '}';
  }

  public Integer getTotalRemainingUnchecked() {
    Integer count = 0;
    for(BingoData[] row : getBingoData()){
      for(BingoData data : row){
        if(!data.isSelected()){
          count += data.getNumber();
        }
      }
    }
    return count;
  }
}
