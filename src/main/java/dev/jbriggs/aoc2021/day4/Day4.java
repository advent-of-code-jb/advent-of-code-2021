package dev.jbriggs.aoc2021.day4;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Day4 extends Day<String> {

  public Day4(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }
    // Get command
    String[] commands = input[0].split(",");

    // Get bingo cards
    List<BingoCard> bingoCards = new ArrayList<>();
    // Loop for every 5 from 2
    for (int i = 2; i < input.length; i += 6) {
      bingoCards.add(
          new BingoCard(
              new String[] {input[i], input[i + 1], input[i + 2], input[i + 3], input[i + 4]}));
    }

    // Start bingo
    for (String command : commands) {
      for(BingoCard card : bingoCards){
        int number = Integer.parseInt(command);
        if(Boolean.TRUE.equals(card.checkNumber(number))){
          return String.valueOf(number * card.getTotalRemainingUnchecked());
        }
      }
    }

    return "0";
  }

  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }
    // Get command
    String[] commands = input[0].split(",");

    // Get bingo cards
    List<BingoCard> bingoCards = new ArrayList<>();
    // Loop for every 5 from 2
    for (int i = 2; i < input.length; i += 6) {
      bingoCards.add(
          new BingoCard(
              new String[] {input[i], input[i + 1], input[i + 2], input[i + 3], input[i + 4]}));
    }

    // Start bingo
    for (String command : commands) {
      for(int i = 0; i < bingoCards.size(); i++){
        int number = Integer.parseInt(command);
        if(Boolean.TRUE.equals(bingoCards.get(i).checkNumber(number))){
          if(bingoCards.size() > 1){
            bingoCards.remove(bingoCards.get(i));
            i = i - 1;
          }else{
            return String.valueOf(number * bingoCards.get(i).getTotalRemainingUnchecked());
          }
        }
      }
    }

    return "0";
  }

  @Override
  public Integer day() {
    return 4;
  }

  @Override
  public String inputPath() {
    return "inputdata/day4/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
