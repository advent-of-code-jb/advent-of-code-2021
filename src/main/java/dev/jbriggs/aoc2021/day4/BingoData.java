package dev.jbriggs.aoc2021.day4;

import lombok.Getter;
import lombok.Setter;

public class BingoData{
  public BingoData(Integer number){
    this.number = number;
    this.selected = false;
  }
  @Getter
  @Setter
  Integer number;
  @Getter
  boolean selected;

  public void checkNumber(){
    selected = true;
  }

  @Override
  public String toString() {
    return "BingoData{" +
        "number=" + number +
        ", selected=" + selected +
        '}';
  }
}
