package dev.jbriggs.aoc2021;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.time.Duration;
import java.time.LocalTime;
import lombok.Data;

@Data
public abstract class Day<T> {

  public Day(PuzzleInputParser puzzleInputParser) {
    this.puzzleInputParser = puzzleInputParser;
  }

  private final PuzzleInputParser puzzleInputParser;

  private T[] input;

  public abstract T[] getInput();

  public abstract String partOne(T[] input);

  public abstract String partTwo(T[] input);

  public abstract Integer day();

  public abstract String inputPath();

  public abstract Type getInputType();

  public boolean skip(){
    return false;
  }

  public enum Type {
    INT,
    STRING
  }

  @Override
  public String toString() {
    if(!this.skip()){
      LocalTime start = LocalTime.now();
      String partOne = partOne(getInput());
      LocalTime checkpoint = LocalTime.now();
      String partTwo = partTwo(getInput());
      LocalTime finish = LocalTime.now();

      return String.format(
          "\nDay %d\n" + "\tPart One: %s (%dms)\n" + "\tPart Two: %s (%dms)\n",
          day(),
          partOne,
          Duration.between(start, checkpoint).toMillis(),
          partTwo,
          Duration.between(checkpoint, finish).toMillis());
    }else{
      return String.format(
          "\nDay %d\n" + "\tPart One: Skipped\n" + "\tPart Two: Skipped\n",
          day());
    }
  }
}
