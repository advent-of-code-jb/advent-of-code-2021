package dev.jbriggs.aoc2021.day13;

import com.google.common.base.Objects;
import dev.jbriggs.aoc2021.AdventOfCodeException;
import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public class PaperFolder {

  public static final int SECOND_GROUP = 2;
  public static final int FIRST_GROUP = 1;

  @Getter
  @Setter
  @AllArgsConstructor
  public static class Fold {
    String type;
    Integer location;

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Fold fold = (Fold) o;
      return Objects.equal(type, fold.type)
          && Objects.equal(location, fold.location);
    }

    @Override
    public int hashCode() {
      return Objects.hashCode(type, location);
    }

    @Override
    public String toString() {
      return "Fold{" +
          "type='" + type + '\'' +
          ", location=" + location +
          '}';
    }
  }

  private static final Pattern DOT_PATTERN = Pattern.compile("^(\\d+),(\\d+)$");
  private static final Pattern FOLD_PATTERN = Pattern.compile("^fold along (\\w+)=(\\d+)$");

  @Getter private final TransparentPaper paper;
  @Getter private final Queue<Fold> folds = new LinkedList<>();

  public PaperFolder(String[] rawDots, String[] rawFolds) {
    if (rawDots.length == 0) {
      throw new AdventOfCodeException("Dots input must not be empty");
    } else if (rawFolds.length == 0) {
      throw new AdventOfCodeException("Fold input must not be empty");
    } else {
      paper = initialisePaper(rawDots);

      for (String rawFold : rawFolds) {
        Matcher matcher = FOLD_PATTERN.matcher(rawFold);
        if (matcher.matches()) {
          folds.add(
              new Fold(matcher.group(FIRST_GROUP), Integer.parseInt(matcher.group(SECOND_GROUP))));
        } else {
          throw new AdventOfCodeException("Wrong format used, should be " + FOLD_PATTERN.pattern());
        }
      }
    }
  }

  private TransparentPaper initialisePaper(String[] rawDots) {
    List<Coordinate> dots = new ArrayList<>();
    for (String rawDot : rawDots) {
      Matcher matcher = DOT_PATTERN.matcher(rawDot);
      if (matcher.matches()) {
        dots.add(new Coordinate(matcher.group(FIRST_GROUP), matcher.group(SECOND_GROUP)));
      } else {
        throw new AdventOfCodeException("Wrong format used, should be " + DOT_PATTERN.pattern());
      }
    }

    return new TransparentPaper(dots.toArray(Coordinate[]::new));
  }

  public void startFold() {
    while(!folds.isEmpty()){
      Fold currentFold = folds.poll();
      if(Objects.equal(currentFold.type, "x")){
        paper.foldOnX(currentFold.location);
      }else if (Objects.equal(currentFold.type, "y")){
        paper.foldOnY(currentFold.location);
      }else{
        throw new AdventOfCodeException("Unknown fold type found");
      }
    }
  }
}
