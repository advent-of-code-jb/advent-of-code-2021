package dev.jbriggs.aoc2021.day13;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day13 extends Day<String> {

  public static final int DAY_NUMBER = 13;
  public static final int TOTAL_STEPS = 100;

  public Day13(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    List<String> rawCoordinates = new ArrayList<>();
    List<String> rawFolds = new ArrayList<>();
    boolean finishedCoordinates = false;
    for (String row : input) {
      if (isEmpty(row)) {
        finishedCoordinates = true;
      } else if (Boolean.FALSE.equals(finishedCoordinates)) {
        rawCoordinates.add(row);
      } else {
        rawFolds.add(row);
      }
    }

    String firstFold = rawFolds.toArray(String[]::new)[0];
    PaperFolder paperFolder =
        new PaperFolder(rawCoordinates.toArray(String[]::new), new String[] {firstFold});

    paperFolder.startFold();

    return String.valueOf(paperFolder.getPaper().getDots().size());
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    List<String> rawCoordinates = new ArrayList<>();
    List<String> rawFolds = new ArrayList<>();
    boolean finishedCoordinates = false;
    for (String row : input) {
      if (isEmpty(row)) {
        finishedCoordinates = true;
      } else if (Boolean.FALSE.equals(finishedCoordinates)) {
        rawCoordinates.add(row);
      } else {
        rawFolds.add(row);
      }
    }

    PaperFolder paperFolder =
        new PaperFolder(rawCoordinates.toArray(String[]::new), rawFolds.toArray(String[]::new));

    paperFolder.startFold();

    return String.valueOf(paperFolder.getPaper().toString());
  }

  @Override
  public Integer day() {
    return DAY_NUMBER;
  }

  @Override
  public String inputPath() {
    return "inputdata/day13/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
