package dev.jbriggs.aoc2021.day13;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class TransparentPaper {
  private final HashSet<Coordinate> dots;

  public TransparentPaper(Coordinate[] coordinates) {
    this.dots = new HashSet<>();
    for (Coordinate coordinate : coordinates) {
      this.addDot(coordinate);
    }
  }

  private void addDot(Coordinate coordinate) {
    this.dots.add(coordinate);
  }

  public Integer getWidth() {
    return this.dots.stream().max(Comparator.comparing(Coordinate::getX)).stream()
        .findFirst()
        .map(x -> x.getX() + 1)
        .orElse(0);
  }

  public Integer getHeight() {
    return this.dots.stream().max(Comparator.comparing(Coordinate::getY)).stream()
        .findFirst()
        .map(x -> x.getY() + 1)
        .orElse(0);
  }

  public void foldOnX(int x) {
    if (x >= getWidth()) {
      throw new AdventOfCodeException("Fold exceeds papers width");
    } else {
      List<Coordinate> toBeFolded =
          new ArrayList<>(this.dots)
              .stream().filter(c -> c.getX() > x).collect(Collectors.toList());
      toBeFolded.forEach(this.dots::remove);
      toBeFolded.forEach(c -> {
        c.setX(x - (c.getX() - x));
        this.addDot(c);
      });
    }
  }

  public void foldOnY(int y) {
    if (y >= getHeight()) {
      throw new AdventOfCodeException("Fold exceeds papers height");
    } else {
      List<Coordinate> toBeFolded =
          new ArrayList<>(this.dots)
              .stream().filter(c -> c.getY() > y).collect(Collectors.toList());
      toBeFolded.forEach(this.dots::remove);
      toBeFolded.forEach(c -> {
        c.setY(y - (c.getY() - y));
        this.addDot(c);
      });
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for(int y = 0; y < getHeight(); y++){
      for(int x = 0; x < getWidth(); x++){
        if(this.dots.contains(new Coordinate(x, y))){
          sb.append("X");
        }else{
          sb.append(" ");
        }
      }
      sb.append("\n");
    }
    return "TransparentPaper{\n" +
        sb.toString() + "\n" +
        '}';
  }
}
