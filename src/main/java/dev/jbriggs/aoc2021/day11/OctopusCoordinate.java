package dev.jbriggs.aoc2021.day11;

import com.google.common.base.Objects;
import dev.jbriggs.aoc2021.common.Coordinate;
import lombok.Getter;

@Getter
public class OctopusCoordinate extends Coordinate {

  private Integer energyLevel;

  public OctopusCoordinate(Integer x, Integer y, Integer energyLevel) {
    super(x, y);
    this.energyLevel = energyLevel;
  }

  public void increaseEnergy(){
    energyLevel++;
  }

  public void resetEnergy(){
    energyLevel = 0;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    OctopusCoordinate that = (OctopusCoordinate) o;
    return Objects.equal(getX(), that.getX())
        && Objects.equal(getY(), that.getY());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(super.hashCode());
  }
}
