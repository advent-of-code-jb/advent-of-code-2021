package dev.jbriggs.aoc2021.day11;

import dev.jbriggs.aoc2021.day9.HeightCoordinate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OctopusCollection {

  public static final int MAX_ENERGY = 9;
  @Getter private final OctopusCoordinate[][] octopus;

  @Getter private int totalFlashes = 0;

  public OctopusCollection(String[] input) {
    octopus = new OctopusCoordinate[input.length][input[0].length()];

    int y = input.length - 1;
    for (String row : input) {
      int x = 0;
      for (char number : row.trim().toCharArray()) {
        octopus[y][x] = new OctopusCoordinate(x, y, Integer.parseInt(String.valueOf(number)));
        x++;
      }
      y--;
    }
  }

  public int getWidth() {
    return octopus.length;
  }

  public int getHeight() {
    return octopus[0].length;
  }

  public Optional<OctopusCoordinate> getOctopusAtPosition(int x, int y) {
    if (x < getWidth() && y < getHeight()) {
      return Optional.of(octopus[y][x]);
    }
    return Optional.empty();
  }

  public int step() {
    increaseAllEnergy();
    HashSet<OctopusCoordinate> octopusToReset = flashAllOctopus();
    totalFlashes += octopusToReset.size();
    resetFlashedOctopus(octopusToReset);
    return octopusToReset.size();
  }

  private void increaseAllEnergy() {
    for (int x = 0; x < getWidth(); x++) {
      for (int y = 0; y < getHeight(); y++) {
        octopus[x][y].increaseEnergy();
      }
    }
  }

  private HashSet<OctopusCoordinate> flashAllOctopus() {
    HashSet<OctopusCoordinate> markedForFlash = new HashSet<>();
    Queue<OctopusCoordinate> toFlash = new LinkedList<>();
    for (int y = getHeight() - 1; y >= 0; y--) {
      for (int x = 0; x < getWidth(); x++) {
        if (octopus[y][x].getEnergyLevel() > MAX_ENERGY) {
          addToFlashQueueIfRequired(markedForFlash, toFlash, octopus[y][x]);
        }
      }
    }
    while (!toFlash.isEmpty()) {
      OctopusCoordinate poll = toFlash.poll();
      increaseNeighbourOctopusEnergy(markedForFlash, toFlash, poll);
    }
    return markedForFlash;
  }

  private static void resetFlashedOctopus(HashSet<OctopusCoordinate> octopusCoordinates) {
    for (OctopusCoordinate octopusCoordinate : octopusCoordinates) {
      octopusCoordinate.resetEnergy();
    }
  }

  private void increaseNeighbourOctopusEnergy(
      HashSet<OctopusCoordinate> markedForFlash,
      Queue<OctopusCoordinate> queuedForFlash,
      OctopusCoordinate source) {
    int x = source.getX();
    int y = source.getY();
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x - 1, y + 1);
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x, y + 1);
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x + 1, y + 1);
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x - 1, y);
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x + 1, y);
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x - 1, y - 1);
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x, y - 1);
    increaseAndFlashIfRequired(markedForFlash, queuedForFlash, x + 1, y - 1);
  }

  private void increaseAndFlashIfRequired(
      HashSet<OctopusCoordinate> markedForFlash,
      Queue<OctopusCoordinate> queuedForFlash,
      int x,
      int y) {
    if ((x >= 0 && x < getWidth()) && (y >= 0 && y < getHeight())) {
      octopus[y][x].increaseEnergy();
      addToFlashQueueIfRequired(markedForFlash, queuedForFlash, octopus[y][x]);
    }
  }

  private static void addToFlashQueueIfRequired(
      HashSet<OctopusCoordinate> markedForFlash,
      Queue<OctopusCoordinate> queuedForFlash,
      OctopusCoordinate octopusCoordinate) {
    if (octopusCoordinate.getEnergyLevel() > MAX_ENERGY
        && !markedForFlash.contains(octopusCoordinate)) {
      markedForFlash.add(octopusCoordinate);
      queuedForFlash.add(octopusCoordinate);
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (int y = getHeight() - 1; y >= 0; y--) {
      for (int x = 0; x < getWidth(); x++) {
        sb.append(octopus[y][x].getEnergyLevel());
      }
      sb.append("\n");
    }
    return sb.toString();
  }
}
