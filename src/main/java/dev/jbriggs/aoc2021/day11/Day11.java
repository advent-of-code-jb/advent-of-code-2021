package dev.jbriggs.aoc2021.day11;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.day10.LineReader;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day11 extends Day<String> {

  public static final int DAY_NUMBER = 11;
  public static final int TOTAL_STEPS = 100;

  public Day11(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    OctopusCollection octopusCollection = new OctopusCollection(input);
    for(int x = 1; x <= TOTAL_STEPS; x++){
      octopusCollection.step();
    }

    return String.valueOf(octopusCollection.getTotalFlashes());
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    OctopusCollection octopusCollection = new OctopusCollection(input);
    int stepNumber = 0;
    int total = 0;
    while(total < 100){
      stepNumber++;
      total = octopusCollection.step();
    }

    return String.valueOf(stepNumber);
  }

  @Override
  public Integer day() {
    return DAY_NUMBER;
  }

  @Override
  public String inputPath() {
    return "inputdata/day11/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
