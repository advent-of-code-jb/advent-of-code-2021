package dev.jbriggs.aoc2021.day3;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class Day3 extends Day<String> {

  public Day3(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    // Integer.parseInt(result.toString(), 2)
    String gammaBinary = getGamma(input);
    String epsilonBinary = getEpsilon(input);

    return String.valueOf(binaryToDecimal(gammaBinary) * binaryToDecimal(epsilonBinary));
  }

  private String getGamma(String[] input) {
    int length = input[0].length();
    StringBuilder result = new StringBuilder();
    List<String> stringInput =
        Arrays.stream(input).map(Object::toString).collect(Collectors.toList());
    for (int i = 0; i < length; i++) {
      int ones = 0;
      int zeros = 0;
      for (String integer : stringInput) {
        if (integer.charAt(i) == '1') {
          ones++;
        } else {
          zeros++;
        }
      }
      if (ones > zeros) {
        result.append("1");
      } else {
        result.append("0");
      }
    }

    return result.toString();
  }

  private String getEpsilon(String[] input) {
    return flipBinary(getGamma(input));
  }

  private String flipBinary(String input) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < input.length(); i++) {
      if (input.charAt(i) == '1') {
        result.append("0");
      } else {
        result.append("1");
      }
    }
    return result.toString();
  }

  private Integer binaryToDecimal(String binary) {
    return Integer.parseInt(binary, 2);
  }

  public String partTwo(String[] input) {
    Integer oxygenGeneratorRating = binaryToDecimal(oxygenGeneratorRating(input));
    Integer co2ScrubberRating = binaryToDecimal(co2ScrubberRating(input));
    return String.valueOf(oxygenGeneratorRating * co2ScrubberRating);
  }

  @Override
  public Integer day() {
    return 3;
  }

  @Override
  public String inputPath() {
    return "inputdata/day3/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }

  public String oxygenGeneratorRating(String[] input) {

    int index = 0;
    List<String> inputList = Arrays.stream(input).collect(Collectors.toList());
    while (inputList.size() != 1) {

      int ones = 0;
      int zeros = 0;
      for (String result : inputList) {
        if (result.charAt(index) == '1') {
          ones++;
        } else {
          zeros++;
        }
      }

      int finalIndex = index;
      if (ones < zeros) {
        inputList.removeIf(x -> x.charAt(finalIndex) == '1');
      } else {
        inputList.removeIf(x -> x.charAt(finalIndex) == '0');
      }

      index++;
    }

    return inputList.get(0);
  }

  public String co2ScrubberRating(String[] input) {
    int index = 0;
    List<String> inputList = Arrays.stream(input).collect(Collectors.toList());
    while (inputList.size() != 1) {

      int ones = 0;
      int zeros = 0;
      for (String result : inputList) {
        if (result.charAt(index) == '1') {
          ones++;
        } else {
          zeros++;
        }
      }

      int finalIndex = index;
      if (zeros <= ones) {
        inputList.removeIf(x -> x.charAt(finalIndex) == '1');
      } else {
        inputList.removeIf(x -> x.charAt(finalIndex) == '0');
      }

      index++;
    }

    return inputList.get(0);
  }
}
