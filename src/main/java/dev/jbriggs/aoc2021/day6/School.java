package dev.jbriggs.aoc2021.day6;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

public class School {

  @Getter private final List<LanternFishPair> lanternFishList = new ArrayList<>();

  @AllArgsConstructor
  public static class LanternFishPair {
    @Getter private LanternFish lanternFish;
    @Getter @Setter private Long total;

    public void incrementTotal() {
      total++;
    }

    public void incrementTotal(Long toIncrement) {
      total += toIncrement;
    }

    public Integer getTimer() {
      return lanternFish.getTimer();
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      LanternFishPair that = (LanternFishPair) o;
      return lanternFish.equals(that.lanternFish);
    }

    @Override
    public int hashCode() {
      return Objects.hash(lanternFish);
    }

    @Override
    public String toString() {
      return "LanternFishPair{" +
          "lanternFish=" + lanternFish +
          ", total=" + total +
          '}';
    }
  }

  public School() {

  }

  @SneakyThrows
  public void addLanternFish(LanternFish lanternFish) {
    if (lanternFishList.stream()
        .anyMatch(x -> Objects.equals(x.getTimer(), lanternFish.getTimer()))) {
      lanternFishList.stream()
          .filter(x -> Objects.equals(x.getTimer(), lanternFish.getTimer()))
          .findFirst()
          .ifPresent(LanternFishPair::incrementTotal);
    } else {
      lanternFishList.add(new LanternFishPair(lanternFish, 1L));
    }
  }

  @SneakyThrows
  public void addLanternFish(LanternFishPair lanternFishPair) {
    if (lanternFishList.stream()
        .anyMatch(x -> Objects.equals(x.getTimer(), lanternFishPair.getTimer()))) {
      lanternFishList.stream()
          .filter(x -> Objects.equals(x.getTimer(), lanternFishPair.getTimer()))
          .findFirst()
          .ifPresent(x -> x.incrementTotal(lanternFishPair.getTotal()));
    } else {
      lanternFishList.add(lanternFishPair);
    }
  }

  private void update() {
    List<LanternFishPair> newFishList = new ArrayList<>();
    // breed fish
    long totalForBreed = getTotalOfFishReadyForBirth();
    if(totalForBreed > 0){
      newFishList.add(new LanternFishPair(new LanternFish(6), totalForBreed)); // Existing fish
      newFishList.add(new LanternFishPair(new LanternFish(8), totalForBreed)); // New fish
    }
    removeParents();

    lanternFishList.stream()
        .sorted(Comparator.comparing(a -> a.lanternFish.getTimer()))
        .forEach(x -> x.lanternFish.update());

    newFishList.forEach(this::addLanternFish);
  }

  private Long getTotalOfFishReadyForBirth() {
    return lanternFishList.stream()
        .filter(x -> x.getTimer().equals(0))
        .findFirst()
        .map(LanternFishPair::getTotal)
        .orElse(0L);
  }

  private void removeParents() {
    lanternFishList.removeIf(x -> x.getTimer().equals(0));
  }

  public void update(int days) {
    for (int x = 0; x < days; x++) {
      update();
    }
  }

  public Long getTotalFish() {
    AtomicReference<Long> count = new AtomicReference<>(0L);
    lanternFishList.forEach(x -> count.updateAndGet(v -> v + x.getTotal()));
    return count.get();
  }
}
