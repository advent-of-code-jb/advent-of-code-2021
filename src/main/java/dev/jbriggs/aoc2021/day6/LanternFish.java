package dev.jbriggs.aoc2021.day6;

import java.util.Objects;
import lombok.Getter;

public class LanternFish {

  public static final int TIMER_ON_BABY_LANTERN_FISH = 8;
  public static final int TIMER_AFTER_GIVING_BIRTH = 6;

  public LanternFish() {
    this.timer = TIMER_ON_BABY_LANTERN_FISH;
  }

  public LanternFish(int birthTime) {
    this.timer = birthTime;
  }

  @Getter private Integer timer;

  public Boolean update() {
    timer--;
    if (timer < 0) {
      timer = TIMER_AFTER_GIVING_BIRTH;
      return true;
    }
    return false;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LanternFish that = (LanternFish) o;
    return timer == that.timer;
  }

  @Override
  public int hashCode() {
    return Objects.hash(timer);
  }

  @Override
  public String toString() {
    return "LanternFish{" +
        "timer=" + timer +
        '}';
  }
}
