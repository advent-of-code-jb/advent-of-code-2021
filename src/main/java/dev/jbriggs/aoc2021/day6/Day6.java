package dev.jbriggs.aoc2021.day6;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day6 extends Day<String> {

  public Day6(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    return getAnswer(input, 80);
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    return getAnswer(input, 256);
  }

  private String getAnswer(String[] input, int days) {
    if (input.length != 1) {
      return "0";
    }
    List<Integer> fish =
        Arrays.stream(input[0].split(",")).map(Integer::parseInt).collect(Collectors.toList());

    School school = new School();
    fish.forEach(x -> school.addLanternFish(new LanternFish(x)));

    school.update(days);

    return String.valueOf(school.getTotalFish());
  }

  @Override
  public Integer day() {
    return 6;
  }

  @Override
  public String inputPath() {
    return "inputdata/day6/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
