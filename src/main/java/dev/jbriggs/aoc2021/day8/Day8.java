package dev.jbriggs.aoc2021.day8;

import static dev.jbriggs.aoc2021.day8.ConnectionSolver.getNumberFromCombinationMap;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.Arrays;
import java.util.Map;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day8 extends Day<String> {

  public Day8(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    String[] fourDigitValues = Arrays.stream(input).map(x -> x.split("\\|")[1]).toArray(String[]::new);

    ConnectionSolver connectionSolver = new ConnectionSolver();
    long count = 0;
    count += connectionSolver.findNumberOfOnes(fourDigitValues);
    count += connectionSolver.findNumberOfFours(fourDigitValues);
    count += connectionSolver.findNumberOfSevens(fourDigitValues);
    count += connectionSolver.findNumberOfEights(fourDigitValues);

    return String.valueOf(count);
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    String[] combinations = Arrays.stream(input).map(x -> x.split("\\|")[0]).toArray(String[]::new);
    String[] fourDigitValues = Arrays.stream(input).map(x -> x.split("\\|")[1]).toArray(String[]::new);

    ConnectionSolver connectionSolver = new ConnectionSolver();

    int count = 0;
    for(int x = 0; x < combinations.length; x++){
      Map<String, Integer> combination = connectionSolver.findConnectionCombinations(combinations[x]);
      String[] numbers = fourDigitValues[x].trim().split("\\s+");
      StringBuilder sb = new StringBuilder();
      sb.append(getNumberFromCombinationMap(combination, numbers[0]));
      sb.append(getNumberFromCombinationMap(combination, numbers[1]));
      sb.append(getNumberFromCombinationMap(combination, numbers[2]));
      sb.append(getNumberFromCombinationMap(combination, numbers[3]));
      count += Integer.parseInt(sb.toString());
    }

    return String.valueOf(count);
  }

  @Override
  public Integer day() {
    return 8;
  }

  @Override
  public String inputPath() {
    return "inputdata/day8/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
