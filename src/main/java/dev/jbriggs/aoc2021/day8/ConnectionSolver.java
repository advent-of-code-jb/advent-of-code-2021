package dev.jbriggs.aoc2021.day8;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConnectionSolver {

  public static final long TWO_CHARACTERS = 2L;
  public static final long THREE_CHARACTERS = 3L;
  public static final long FOUR_CHARACTERS = 4L;
  public static final long SEVEN_CHARACTERS = 7L;

  public Long findNumberOfOnes(String[] strings) {
    return findNumberOfOccurrencesSpecificLengthCombinationsExists(strings, TWO_CHARACTERS);
  }

  public Long findNumberOfFours(String[] strings) {
    return findNumberOfOccurrencesSpecificLengthCombinationsExists(strings, FOUR_CHARACTERS);
  }

  public Long findNumberOfSevens(String[] strings) {
    return findNumberOfOccurrencesSpecificLengthCombinationsExists(strings, THREE_CHARACTERS);
  }

  public Long findNumberOfEights(String[] strings) {
    return findNumberOfOccurrencesSpecificLengthCombinationsExists(strings, SEVEN_CHARACTERS);
  }

  public Map<String, Integer> findConnectionCombinations(String connections) {
    Map<String, Integer> result = new HashMap<>();

    // Easy numbers
    String numberOne = getNumberOne(connections);
    String numberFour = getNumberFour(connections);
    String numberSeven = getNumberSeven(connections);
    String numberEight = getNumberEight(connections);
    // Harder numbers
    String numberTwo = getNumberTwo(connections);
    String numberThree = getNumberThree(connections);
    String numberFive = getNumberFive(connections);
    String numberSix = getNumberSix(connections);
    String numberNine = getNumberNine(connections);
    String numberZero = getNumberZero(connections);

    result.put(numberZero, 0);
    result.put(numberOne, 1);
    result.put(numberTwo, 2);
    result.put(numberThree, 3);
    result.put(numberFour, 4);
    result.put(numberFive, 5);
    result.put(numberSix, 6);
    result.put(numberSeven, 7);
    result.put(numberEight, 8);
    result.put(numberNine, 9);

    return result;
  }

  private static Long findNumberOfOccurrencesSpecificLengthCombinationsExists(
      String[] strings, long length) {
    if (strings.length == 0) {
      return 0L;
    }
    long count = 0L;
    for (String row : strings) {
      count += getCharactersOfLength(row, length).count();
    }
    return count;
  }

  private static Stream<String> getCharactersOfLength(String row, long length) {
    return Arrays.stream(row.split("\\s+")).filter(x -> x.length() == length);
  }

  private static String getNumberZero(String combinations) {
    return inAlphabeticalOrder(
        getCharactersOfLength(combinations, 6)
            .filter(
                x ->
                    !stringContainsCharacters(x, getNumberSix(combinations).toCharArray())
                        && !stringContainsCharacters(x, getNumberNine(combinations).toCharArray()))
            .findFirst()
            .orElse(""));
  }

  private static String getNumberOne(String combinations) {
    return inAlphabeticalOrder(getCharactersOfLength(combinations, 2).findFirst().orElse(""));
  }

  private static String getNumberTwo(String combinations) {
    return inAlphabeticalOrder(
        getCharactersOfLength(combinations, 5)
            .filter(
                x ->
                    stringContainsNumberOfCharacters(
                        x, getNumberFour(combinations).toCharArray(), 2))
            .findFirst()
            .orElse(""));
  }

  private static String getNumberThree(String combinations) {
    return inAlphabeticalOrder(
        getCharactersOfLength(combinations, 5)
            .filter(x -> stringContainsCharacters(x, getNumberOne(combinations).toCharArray()))
            .findFirst()
            .orElse(""));
  }

  private static String getNumberFour(String combinations) {
    return inAlphabeticalOrder(getCharactersOfLength(combinations, 4).findFirst().orElse(""));
  }

  private static String getNumberFive(String combinations) {
    return inAlphabeticalOrder(
        getCharactersOfLength(combinations, 5)
            .filter(
                x ->
                    !stringContainsCharacters(x, getNumberTwo(combinations).toCharArray())
                        && !stringContainsCharacters(x, getNumberThree(combinations).toCharArray()))
            .findFirst()
            .orElse(""));
  }

  private static String getNumberSix(String combinations) {
    return inAlphabeticalOrder(
        getCharactersOfLength(combinations, 6)
            .filter(
                x ->
                    stringContainsNumberOfCharacters(
                        x, getNumberOne(combinations).toCharArray(), 1))
            .findFirst()
            .orElse(""));
  }

  private static String getNumberSeven(String combinations) {
    return inAlphabeticalOrder(getCharactersOfLength(combinations, 3).findFirst().orElse(""));
  }

  private static String getNumberEight(String combinations) {
    return inAlphabeticalOrder(getCharactersOfLength(combinations, 7).findFirst().orElse(""));
  }

  private static String getNumberNine(String combinations) {
    return inAlphabeticalOrder(
        getCharactersOfLength(combinations, 6)
            .filter(x -> stringContainsCharacters(x, getNumberFour(combinations).toCharArray()))
            .findFirst()
            .orElse(""));
  }

  private static String findUncommonCharacterInStrings(String targetString, String requestString) {
    for (char character : requestString.toCharArray()) {
      String characterString = String.valueOf(character);
      if (!targetString.contains(characterString)) {
        return characterString;
      }
    }
    return "";
  }

  private static String findCommonCharacterInStrings(String targetString, String requestString) {
    for (char character : requestString.toCharArray()) {
      String characterString = String.valueOf(character);
      if (targetString.contains(characterString)) {
        return characterString;
      }
    }
    return "";
  }

  private static String inAlphabeticalOrder(String targetString) {
    char charArray[] = targetString.toCharArray();
    Arrays.sort(charArray);
    return new String(charArray);
  }

  private static Boolean stringContainsCharacters(String string, char[] chars) {
    return stringContainsNumberOfCharacters(string, chars, chars.length);
  }

  private static Boolean stringContainsNumberOfCharacters(
      String string, char[] chars, int numberOfCharacters) {
    int count = 0;
    for (char character : chars) {
      if (string.contains(String.valueOf(character))) {
        count++;
      }
    }
    return count == numberOfCharacters;
  }

  public static int getNumberFromCombinationMap(Map<String, Integer> map, String combination) {
    if (map.containsKey(inAlphabeticalOrder(combination))) {
      return map.get(inAlphabeticalOrder(combination));
    }

    log.error("Combination {} does not exist in map {}", combination, map);
    return -1;
  }
}
