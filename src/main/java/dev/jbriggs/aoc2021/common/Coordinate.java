package dev.jbriggs.aoc2021.common;

import com.google.common.base.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Coordinate {
  private Integer x;
  private Integer y;

  public Coordinate(String x, String y){
    this.x = Integer.parseInt(x);
    this.y = Integer.parseInt(y);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Coordinate that = (Coordinate) o;
    return Objects.equal(x, that.x) && Objects.equal(y, that.y);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(x, y);
  }

  @Override
  public String toString() {
    return String.format("(%d, %d)", this.x, this.y);
  }
}
