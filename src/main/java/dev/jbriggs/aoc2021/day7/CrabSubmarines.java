package dev.jbriggs.aoc2021.day7;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class CrabSubmarines {
  @Getter
  List<Integer> crabLocations = new ArrayList<>();

  public void enterCrabLocations(String[] locations) {
    for (String location : locations){
      crabLocations.add(Integer.parseInt(location));
    }
  }

  public Integer getLeftMostPosition() {
    return crabLocations.stream().min(Integer::compareTo).orElse(null);
  }

  public Integer getRightMostPosition() {
    return crabLocations.stream().max(Integer::compareTo).orElse(null);
  }
}
