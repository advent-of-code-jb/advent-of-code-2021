package dev.jbriggs.aoc2021.day7;

import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class FuelCalculator {

  public static Integer calculateEfficientFuel(CrabSubmarines crabSubmarines) {
    return getBestLocation(
        crabSubmarines.getCrabLocations(),
        crabSubmarines.getLeftMostPosition(),
        crabSubmarines.getRightMostPosition());
  }

  private static int getBestLocation(
      List<Integer> crabSubmarines, int leftMostPosition, int rightMostPosition) {
    int result = Integer.MAX_VALUE;
    for (int locationBeingChecked = leftMostPosition;
        locationBeingChecked <= rightMostPosition;
        locationBeingChecked++) {
      int total = 0;
      for (Integer crabLocation : crabSubmarines) {
        int toAdd = 0;
        if (crabLocation > locationBeingChecked) {
          toAdd += (crabLocation - locationBeingChecked);
        } else if (crabLocation < locationBeingChecked) {
          toAdd += (locationBeingChecked - crabLocation);
        }
        total += toAdd;
      }
      if (total < result) {
        result = total;
      }
    }
    return result;
  }

  public static Long calculateCrabEfficientFuel(CrabSubmarines crabSubmarines) {
    return getCrabBestLocation(
        crabSubmarines.getCrabLocations(),
        crabSubmarines.getLeftMostPosition(),
        crabSubmarines.getRightMostPosition());
  }

  private static Long getCrabBestLocation(
      List<Integer> crabSubmarines, int leftMostPosition, int rightMostPosition) {
    Long result = Long.MAX_VALUE;
    for (int locationBeingChecked = leftMostPosition;
        locationBeingChecked <= rightMostPosition;
        locationBeingChecked++) {
      Long total = 0L;
      for (Long crabLocation : crabSubmarines.stream().mapToLong(Integer::longValue).toArray()) {
        Long toAdd = 0L;
        if (crabLocation > locationBeingChecked) {
          toAdd += getTriangularNumber(crabLocation - locationBeingChecked);
        } else if (crabLocation < locationBeingChecked) {
          toAdd += getTriangularNumber(locationBeingChecked - crabLocation);
        }
        total += toAdd;
      }
      if (total < result) {
        result = total;
      }
    }
    return result;
  }

  private static Integer getTriangularNumber(Long at) {
    return Math.round(((at) * (at + 1)) / 2f);
  }
}
