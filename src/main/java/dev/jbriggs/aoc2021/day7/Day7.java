package dev.jbriggs.aoc2021.day7;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day7 extends Day<String> {

  public Day7(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length != 1) {
      return "0";
    }

    CrabSubmarines crabSubmarines = new CrabSubmarines();
    crabSubmarines.enterCrabLocations(input[0].split(","));

    return FuelCalculator.calculateEfficientFuel(crabSubmarines).toString();
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length != 1) {
      return "0";
    }

    CrabSubmarines crabSubmarines = new CrabSubmarines();
    crabSubmarines.enterCrabLocations(input[0].split(","));

    return FuelCalculator.calculateCrabEfficientFuel(crabSubmarines).toString();
  }

  @Override
  public Integer day() {
    return 7;
  }

  @Override
  public String inputPath() {
    return "inputdata/day7/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
