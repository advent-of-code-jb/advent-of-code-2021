package dev.jbriggs.aoc2021.day1;

import static java.util.Objects.isNull;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Day1 extends Day<Integer> {

  public Day1(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public Integer[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFileInteger(inputPath());
  }

  public String partOne(Integer[] input){

    int numberOfIncrements = 0;
    Integer previous = null;
    for(Integer current : input){
      if(!isNull(previous)){
        // After first time encounter
        if(current > previous){
          numberOfIncrements++;
        }
      }
      // First time encounter
      previous = current;
    }

    return Integer.toString(numberOfIncrements);
  }

  public String partTwo(Integer[] input) {
    // Make array using windows
    List<Integer> newInput = new ArrayList<>();
    for(int i = 0; i < input.length - 2; i++){
      newInput.add(input[i] + input[i+1] + input[i+2]);
    }
    return partOne(newInput.toArray(new Integer[0]));
  }

  @Override
  public Integer day() {
    return 1;
  }

  @Override
  public String inputPath() {
    return "inputdata/day1/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.INT;
  }
}
