package dev.jbriggs.aoc2021.day14;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day14 extends Day<String> {

  public static final int DAY_NUMBER = 14;
  public static final int TEN_STEPS = 10;
  public static final int SECOND_PART_OF_INPUT_STARTING_INDEX = 2;
  public static final int FOURTY_STEPS = 40;

  public Day14(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    String rawTemplate = input[0];

    List<PairInsertionRule> rules = new ArrayList<>();
    for (int x = SECOND_PART_OF_INPUT_STARTING_INDEX; x < input.length; x++) {
      rules.add(new PairInsertionRule(input[x]));
    }

    PolymerTemplate polymerTemplate = new PolymerTemplate(rawTemplate, rules);

    for (int x = 0; x < TEN_STEPS; x++) {
      polymerTemplate.step();
    }

    return String.valueOf(
        polymerTemplate.countMostOccuringCharacter()
            - polymerTemplate.countLeastOccuringCharacter());
  }

  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    String rawTemplate = input[0];

    List<PairInsertionRule> rules = new ArrayList<>();
    for (int x = SECOND_PART_OF_INPUT_STARTING_INDEX; x < input.length; x++) {
      rules.add(new PairInsertionRule(input[x]));
    }

    PolymerTemplate polymerTemplate = new PolymerTemplate(rawTemplate, rules);

    for (int x = 0; x < FOURTY_STEPS; x++) {
      log.debug("Step {}", x);
      polymerTemplate.step();
    }

    return String.valueOf(
        polymerTemplate.countMostOccuringCharacter()
            - polymerTemplate.countLeastOccuringCharacter());
  }

  @Override
  public Integer day() {
    return DAY_NUMBER;
  }

  @Override
  public String inputPath() {
    return "inputdata/day14/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
