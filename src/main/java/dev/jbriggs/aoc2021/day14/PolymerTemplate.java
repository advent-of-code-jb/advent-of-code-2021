package dev.jbriggs.aoc2021.day14;

import static com.google.common.base.Strings.isNullOrEmpty;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import lombok.Getter;

public class PolymerTemplate {

  // private String template;
  @Getter private HashMap<String, Long> templatePairs;
  @Getter private List<PairInsertionRule> rules = new ArrayList<>();

  private void addToTemplatePairs(String pair) {
    addToHashMapCount(pair, templatePairs);
  }

  private void addToHashMapCount(String pair, HashMap<String, Long> pairs) {
    addToHashMapCount(pair, pairs, 1L);
  }

  private void addToHashMapCount(String pair, HashMap<String, Long> pairs, Long toAdd) {
    Long count = pairs.containsKey(pair) ? (pairs.get(pair) + toAdd) : toAdd;
    pairs.put(pair, count);
  }

  public PolymerTemplate(final String rawTemplate) {
    if (isNullOrEmpty(rawTemplate)) {
      throw new AdventOfCodeException("Template must not be empty");
    } else {
      this.templatePairs = new HashMap<>();
      for (int x = 0; x < rawTemplate.length() - 1; x++) {
        addToTemplatePairs(rawTemplate.substring(x, x + 2));
      }
    }
  }

  public PolymerTemplate(String template, List<PairInsertionRule> rules) {
    this(template);
    if (rules.isEmpty()) {
      throw new AdventOfCodeException("Rules list must contain rules");
    }
    this.rules = new ArrayList<>(rules);
  }

  public void step() {
    HashMap<String, Long> newTemplates = new HashMap<>();
    for (String pair : templatePairs.keySet()) {
      Long count = templatePairs.get(pair);
      Optional<PairInsertionRule> rule =
          rules.stream().filter(x -> Objects.equals(pair, x.getRule())).findFirst();

      if (rule.isPresent()) {
        addToHashMapCount(pair.charAt(0) + rule.get().getResult(), newTemplates, count);
        addToHashMapCount(rule.get().getResult() + pair.charAt(1), newTemplates, count);
      } else {
        addToHashMapCount(pair, newTemplates);
      }
    }
    templatePairs = new HashMap<>(newTemplates);
  }

  public Long countMostOccuringCharacter() {
    return getCharacterHashMap().values().stream().max(Long::compareTo).orElse(Long.MAX_VALUE);
  }

  public Long countLeastOccuringCharacter() {
    return getCharacterHashMap().values().stream().min(Long::compareTo).orElse(Long.MIN_VALUE);
  }

  private HashMap<String, Long> getCharacterHashMap() {
    HashMap<String, Long> allCharacters = new HashMap<>();
    String[] pairs = templatePairs.keySet().toArray(new String[0]);
    for (String pair : pairs) {
      Long count = templatePairs.get(pair);
      addToHashMapCount(String.valueOf(pair.charAt(0)), allCharacters, count);
    }
    addToHashMapCount(String.valueOf(pairs[pairs.length - 1].charAt(1)), allCharacters);
    return allCharacters;
  }
}
