package dev.jbriggs.aoc2021.day14;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PairInsertionRule {

  private static final Pattern PAIR_INSERTION_RULE_PATTERN = Pattern.compile("^(\\w+) -> (\\w+)$");
  public static final int RULE_GROUP_INDEX = 1;
  public static final int RESULT_GROUP_INDEX = 2;

  private final String rule;
  private final String result;

  public PairInsertionRule(String rawRule) {
    Matcher matcher = PAIR_INSERTION_RULE_PATTERN.matcher(rawRule);
    if (matcher.matches()){
      rule = matcher.group(RULE_GROUP_INDEX);
      result = matcher.group(RESULT_GROUP_INDEX);
    }else{
      throw new AdventOfCodeException("Wrong pattern inserted");
    }
  }
}
