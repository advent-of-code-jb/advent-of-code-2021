package dev.jbriggs.aoc2021.day12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class CaveSystem {

  private final HashSet<CavePath> paths = new HashSet<>();

  public void addPath(Cave caveA, Cave caveB) {
    if (caveB.getCaveType() != CaveType.START && caveA.getCaveType() != CaveType.END) {
      paths.add(new CavePath(caveA, caveB));
    }
    if (caveA.getCaveType() != CaveType.START && caveB.getCaveType() != CaveType.END) {
      paths.add(new CavePath(caveB, caveA));
    }
  }

  private List<CavePath> getPathWithOrigin(String origin) {
    return paths.stream()
        .filter(x -> x.getStart().getName().equals(origin))
        .collect(Collectors.toList());
  }

  private List<CavePath> getPathWithOrigin(Cave origin) {
    return getPathWithOrigin(origin.getName());
  }

  private final List<Cave> currentPath = new LinkedList<>();
  private final List<List<Cave>> allPaths = new ArrayList<>();
  private final HashMap<Cave, Integer> visited = new HashMap<>();

  private void dfs(Cave u, Cave v) {
    dfs(u, v, 1);
  }

  private boolean hasReachedVisitedSmallCaveLimit(int totalTimesVisitingSmallCave){
    List<Cave> smallCaves = new ArrayList<>();
    for(Cave cave : currentPath){
      if(cave.getCaveType().equals(CaveType.SMALL)){
        smallCaves.add(cave);
        if(smallCaves.stream().filter(x -> x.getName().equals(cave.getName())).count() >= totalTimesVisitingSmallCave){
          return true;
        }
      }
    }
    return false;
  }

  private void dfs(Cave u, Cave v, Integer totalTimesVisitingSmallCave) {
    if (visited.containsKey(u) && (hasReachedVisitedSmallCaveLimit(totalTimesVisitingSmallCave)
        || visited.get(u) >= totalTimesVisitingSmallCave)) {
      return;
    }

    if (u.getCaveType() == CaveType.SMALL) {
      if (visited.containsKey(u) && !hasReachedVisitedSmallCaveLimit(totalTimesVisitingSmallCave)) {
        visited.put(u, visited.get(u) + 1);
      } else {
        visited.put(u, 1);
      }
    }

    currentPath.add(u);
    if (Objects.equals(u, v)) {
      allPaths.add(new ArrayList<>(currentPath));
      removeFromVisited(u);
      removeLastPath(currentPath);
      return;
    }

    for (CavePath path : getPathWithOrigin(u)) {
      dfs(path.getFinish(), v, totalTimesVisitingSmallCave);
    }

    removeLastPath(currentPath);
    removeFromVisited(u);
  }

  private void removeFromVisited(Cave u) {
    if (visited.containsKey(u)) {
      if (visited.get(u) == 1) {
        visited.remove(u);
      } else {
        visited.put(u, visited.get(u) - 1);
      }
    }
  }

  private Cave removeLastPath(List<Cave> currentPath) {
    return currentPath.remove(currentPath.size() - 1);
  }

  public List<String> getAllPossiblePaths() {
    return getAllPossiblePaths(1);
  }

  public List<String> getAllPossiblePaths(int totalTimesVisitingSmallCave) {

    Cave start = new Cave("start");
    Cave end = new Cave("end");

    dfs(start, end, totalTimesVisitingSmallCave);

    HashSet<String> result = new HashSet<>();
    for (List<Cave> path : allPaths) {
      StringBuilder sb = new StringBuilder();
      for (Cave cave : path) {
        sb.append(cave.getName());
        if (!cave.equals(end)) {
          sb.append(",");
        }
      }
      result.add(sb.toString());
    }
    return new ArrayList<>(result);
  }
}
