package dev.jbriggs.aoc2021.day12;

import java.util.Locale;
import java.util.Objects;
import lombok.Getter;

@Getter
public class Cave {
  private final String name;

  public Cave(String name) {
    this.name = name;
  }

  public CaveType getCaveType(){
    if(Objects.equals(name, "start")){
      return CaveType.START;
    }else if(Objects.equals(name, "end")){
      return CaveType.END;
    }else{
      return Objects.equals(name, name.toUpperCase(Locale.ROOT)) ? CaveType.LARGE : CaveType.SMALL;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Cave cave = (Cave) o;
    return com.google.common.base.Objects.equal(name, cave.name);
  }

  @Override
  public int hashCode() {
    return com.google.common.base.Objects.hashCode(name);
  }

  @Override
  public String toString() {
    return "Cave{" +
        "name='" + name + '\'' +
        '}';
  }
}
