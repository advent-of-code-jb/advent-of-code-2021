package dev.jbriggs.aoc2021.day12;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import java.util.Objects;
import lombok.Getter;

@Getter
public class CavePath {
  private final Cave start;
  private final Cave finish;

  public CavePath(Cave start, Cave finish) {
    if (Objects.equals(start.getName(), "finish")) {
      throw new AdventOfCodeException("'Finish' cave can not be start point");
    } else if (Objects.equals(finish.getName(), "start")) {
      throw new AdventOfCodeException("'Start' cave can not be finish point");
    } else {
      this.start = start;
      this.finish = finish;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CavePath cavePath = (CavePath) o;
    return com.google.common.base.Objects.equal(start, cavePath.start)
        && com.google.common.base.Objects.equal(finish, cavePath.finish);
  }

  @Override
  public int hashCode() {
    return com.google.common.base.Objects.hashCode(start, finish);
  }

  @Override
  public String toString() {
    return "CavePath{" + "startFinish=" + start + "-" + finish + '}';
  }
}
