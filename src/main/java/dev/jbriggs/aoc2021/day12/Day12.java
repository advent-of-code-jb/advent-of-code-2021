package dev.jbriggs.aoc2021.day12;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.List;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day12 extends Day<String> {

  public static final int DAY_NUMBER = 12;
  public static final int TOTAL_STEPS = 100;

  public Day12(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    CaveSystem caveSystem = new CaveSystem();
    for(String rawPath : input){
      String[] startFinish = rawPath.split("-");
      Cave start = new Cave(startFinish[0]);
      Cave finish = new Cave(startFinish[1]);
      caveSystem.addPath(start, finish);
    }

    return String.valueOf(caveSystem.getAllPossiblePaths().size());
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    CaveSystem caveSystem = new CaveSystem();
    for(String rawPath : input){
      String[] startFinish = rawPath.split("-");
      Cave start = new Cave(startFinish[0]);
      Cave finish = new Cave(startFinish[1]);
      caveSystem.addPath(start, finish);
    }

    List<String> allPossiblePaths = caveSystem.getAllPossiblePaths(2);
    return String.valueOf(allPossiblePaths.size());
  }

  @Override
  public Integer day() {
    return DAY_NUMBER;
  }

  @Override
  public String inputPath() {
    return "inputdata/day12/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
