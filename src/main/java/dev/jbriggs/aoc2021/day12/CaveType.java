package dev.jbriggs.aoc2021.day12;

public enum CaveType {
  START,
  END,
  LARGE,
  SMALL
}
