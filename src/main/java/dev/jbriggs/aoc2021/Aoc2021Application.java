package dev.jbriggs.aoc2021;

import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@RequiredArgsConstructor
public class Aoc2021Application implements CommandLineRunner {

  private final PuzzleInputParser puzzleInputParser;
  private final List<Day> days;

  public static void main(String[] args) {
    log.info("STARTING THE APPLICATION");
    SpringApplication.run(Aoc2021Application.class, args).close();

    log.info("APPLICATION FINISHED");
  }

  @Override
  public void run(String... args) {
    log.info("EXECUTING : command line runner");

    for (Day day :
        days.stream().sorted(Comparator.comparing(Day::day)).collect(Collectors.toList())) {
      Object[] dayInput = null;
      if (day.getInputType() == Day.Type.INT) {
        // Day
        dayInput = puzzleInputParser.getPuzzleInputFromFileInteger(day.inputPath());
      } else {
        dayInput = puzzleInputParser.getPuzzleInputFromFile(day.inputPath());
      }
      day.setInput(dayInput);
      log.info(day.toString());
    }
  }
}
