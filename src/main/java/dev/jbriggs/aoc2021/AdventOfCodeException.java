package dev.jbriggs.aoc2021;

public class AdventOfCodeException extends RuntimeException {
  public AdventOfCodeException(String s) {
    super(s);
  }
}
