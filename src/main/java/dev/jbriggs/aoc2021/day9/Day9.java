package dev.jbriggs.aoc2021.day9;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Day9 extends Day<String> {

  public static final int DAY_NUMBER = 9;

  public Day9(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public boolean skip() {
    return true;
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  @SneakyThrows
  public String partOne(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    CaveGrid caveGrid = new CaveGrid(input);

    int result =
        caveGrid.getLowPoints().stream()
            .map(HeightCoordinate::getRiskLevel)
            .mapToInt(Integer::intValue)
            .sum();

    return Integer.toString(result);
  }

  @SneakyThrows
  public String partTwo(String[] input) {
    if (input.length == 0) {
      return "0";
    }

    CaveGrid caveGrid = new CaveGrid(input);

    List<Basin> basins =
        caveGrid.getBasins().stream()
            .sorted(Comparator.comparingInt(Basin::getSize).reversed())
            .collect(Collectors.toList());
    int result = basins.get(0).getSize() * basins.get(1).getSize() * basins.get(2).getSize();
    return Integer.toString(result);
  }

  @Override
  public Integer day() {
    return DAY_NUMBER;
  }

  @Override
  public String inputPath() {
    return "inputdata/day9/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
