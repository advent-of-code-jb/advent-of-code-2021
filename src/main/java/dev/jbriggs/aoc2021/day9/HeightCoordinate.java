package dev.jbriggs.aoc2021.day9;

import com.google.common.base.Objects;
import dev.jbriggs.aoc2021.common.Coordinate;
import java.io.Serializable;
import lombok.Getter;

public class HeightCoordinate extends Coordinate
    implements Comparable<HeightCoordinate>, Serializable {

  @Getter private final int height;

  public HeightCoordinate(Integer x, Integer y, int height) {
    super(x, y);
    this.height = height;
  }

  public HeightCoordinate(HeightCoordinate heightCoordinate) {
    super(heightCoordinate.getX(), heightCoordinate.getY());
    this.height = heightCoordinate.getHeight();
  }

  public int getRiskLevel() {
    return height + 1;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    HeightCoordinate that = (HeightCoordinate) o;
    return Objects.equal(height, that.height)
        && Objects.equal(getX(), that.getX())
        && Objects.equal(getY(), that.getY());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(super.hashCode(), height);
  }

  @Override
  public String toString() {
    return String.format("(%d, %d) -> %d", this.getX(), this.getY(), height);
  }

  @Override
  public int compareTo(HeightCoordinate o) {
    return Integer.compare(this.height, o.getHeight());
  }
}
