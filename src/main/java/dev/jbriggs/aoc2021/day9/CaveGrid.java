package dev.jbriggs.aoc2021.day9;

import dev.jbriggs.aoc2021.AdventOfCodeException;
import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.SneakyThrows;

public class CaveGrid {

  public static final int NON_BASIN_HEIGHT = 9;
  @Getter private final List<HeightCoordinate> heightCoordinates;

  public CaveGrid(String[] heightMapInput) {
    heightCoordinates = new ArrayList<>();
    int y = heightMapInput.length - 1;
    for (String input : heightMapInput) {
      int x = 0;
      for (char number : input.trim().toCharArray()) {
        heightCoordinates.add(new HeightCoordinate(x, y, Integer.parseInt(String.valueOf(number))));
        x++;
      }
      y--;
    }
  }

  /**
   * Method to return the width of the Cave Grid.
   *
   * @return The width of cave grid (will return 3 if x coordinates are 0, 1, 2).
   */
  public int getGridWidth() {
    return heightCoordinates.stream()
        .max(Comparator.comparing(Coordinate::getX))
        .map(x -> x.getX() + 1)
        .orElse(0);
  }

  /**
   * Method to return the height of the Cave Grid.
   *
   * @return the height of cave grid (will return 3 if y coordinates are 0, 1, 2).
   */
  public int getGridHeight() {
    return heightCoordinates.stream()
        .max(Comparator.comparing(Coordinate::getY))
        .map(x -> x.getY() + 1)
        .orElse(0);
  }

  /**
   * Method to find specific coordinate in grid.
   *
   * @param x X coordinate
   * @param y Y coordinate
   * @return Optional of coordinate found in grid. Returns empty Optional if coordinate not found.
   */
  public Optional<HeightCoordinate> getHeightCoordinate(int x, int y) {
    return heightCoordinates.stream()
        .filter(a -> Objects.equals(a.getX(), x) && Objects.equals(a.getY(), y))
        .findFirst();
  }

  /**
   * Returns neighbours of given HeightCoordinate.
   *
   * @param heightCoordinate Origin HeightCoordinate.
   * @return A list of HeightCoordinates
   */
  public List<HeightCoordinate> getNeighbours(final Coordinate heightCoordinate) {
    List<HeightCoordinate> result = new ArrayList<>();
    getHeightCoordinate(heightCoordinate.getX(), heightCoordinate.getY() + 1)
        .ifPresent(result::add);
    getHeightCoordinate(heightCoordinate.getX(), heightCoordinate.getY() - 1)
        .ifPresent(result::add);
    getHeightCoordinate(heightCoordinate.getX() - 1, heightCoordinate.getY())
        .ifPresent(result::add);
    getHeightCoordinate(heightCoordinate.getX() + 1, heightCoordinate.getY())
        .ifPresent(result::add);
    return result;
  }

  /**
   * Method to find if given coordinate is a low point or not. A coordinate is defined as a low
   * point if the coordinates up, down, left, and right have a higher height. If coordinate is at
   * the edge of a grid, the missing neighbour coordinate does not count.
   *
   * @param heightCoordinate Coordinate to check if low point or not.
   * @return Will return True if coordinates up, down, left, and right of this coordinate has a
   *     higher height
   */
  public Boolean isCoordinateLowPoint(HeightCoordinate heightCoordinate) {
    Boolean upHigher =
        getHeightCoordinate(heightCoordinate.getX(), heightCoordinate.getY() + 1)
            .map(x -> x.getHeight() > heightCoordinate.getHeight())
            .orElse(true);
    Boolean downHigher =
        getHeightCoordinate(heightCoordinate.getX(), heightCoordinate.getY() - 1)
            .map(x -> x.getHeight() > heightCoordinate.getHeight())
            .orElse(true);
    Boolean leftHigher =
        getHeightCoordinate(heightCoordinate.getX() - 1, heightCoordinate.getY())
            .map(x -> x.getHeight() > heightCoordinate.getHeight())
            .orElse(true);
    Boolean rightHigher =
        getHeightCoordinate(heightCoordinate.getX() + 1, heightCoordinate.getY())
            .map(x -> x.getHeight() > heightCoordinate.getHeight())
            .orElse(true);
    return Boolean.TRUE.equals(upHigher)
        && Boolean.TRUE.equals(downHigher)
        && Boolean.TRUE.equals(leftHigher)
        && Boolean.TRUE.equals(rightHigher);
  }

  /**
   * Returns a list of low points within the CaveGrid object. A HeightCoordinate is deemed a low
   * point when all 4 directions of the heightCoordinate have a higher height.
   *
   * @return A list of HeightCoordinates
   */
  @SneakyThrows
  public List<HeightCoordinate> getLowPoints() {
    List<HeightCoordinate> result = new ArrayList<>();
    for (int x = 0; x < this.getGridWidth(); x++) {
      for (int y = 0; y < this.getGridHeight(); y++) {

        Optional<HeightCoordinate> heightCoordinate = this.getHeightCoordinate(x, y);
        if (heightCoordinate.isEmpty()) {
          throw new AdventOfCodeException("Height coordinate should exist");
        }

        if (Boolean.TRUE.equals(this.isCoordinateLowPoint(heightCoordinate.get()))) {
          result.add(heightCoordinate.get());
        }
      }
    }

    return result;
  }

  /**
   * Returns a list of basins within the CaveGrid object. A Basin is a list of coordinates which are
   * grouped together and are bound by coordinates with a height of 9.
   *
   * @return A list of Basin
   */
  public List<Basin> getBasins() {
    List<Basin> collect = getLowPoints().stream().map(Basin::new).collect(Collectors.toList());

    for (Basin basin : collect) {
      Queue<HeightCoordinate> toAddToBasin = new PriorityQueue<>(getNeighbours(basin.getLowPoint()));
      while (!toAddToBasin.isEmpty()){
        HeightCoordinate toAdd = toAddToBasin.poll();
        if(toAdd.getHeight() != NON_BASIN_HEIGHT){
          basin.addCoordinate(new HeightCoordinate(toAdd));
          for(HeightCoordinate coordinate : getNeighbours(toAdd)){
            if(!basin.doesCoordinateExist(coordinate)){
              toAddToBasin.add(new HeightCoordinate(coordinate));
            }
          }
        }
      }
    }

    return collect;
  }
}
