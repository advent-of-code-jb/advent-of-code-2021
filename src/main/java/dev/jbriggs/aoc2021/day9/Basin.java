package dev.jbriggs.aoc2021.day9;

import dev.jbriggs.aoc2021.common.Coordinate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.Getter;

public class Basin {

  @Getter private final List<Coordinate> coordinates = new ArrayList<>();

  @Getter private HeightCoordinate lowPoint;

  public Basin(HeightCoordinate lowPoint) {
    coordinates.add(lowPoint);
    this.lowPoint = lowPoint;
  }

  /**
   * Adds a coordinate to the Basin.
   *
   * @param heightCoordinate HeightCoordinate to add to Basin.
   */
  public void addCoordinate(HeightCoordinate heightCoordinate) {
    if(!doesCoordinateExist(heightCoordinate)){
      coordinates.add(heightCoordinate);
    }
  }

  /**
   * Method to return the size of the Basin. This is based on how many coordinates are found within
   * the Basin.
   *
   * @return Size of the Basin.
   */
  public int getSize() {
    return coordinates.size();
  }

  /**
   * Should return a boolean to let the user know if a specific coordinate exists in a basin.
   * This is based on X and Y coordinates as apposed to the object itself.
   *
   * @param coordinate The coordinate to use as reference.
   * @return Boolean value.
   */
  public boolean doesCoordinateExist(Coordinate coordinate) {
    return coordinates.stream()
        .anyMatch(
            x ->
                Objects.equals(x.getX(), coordinate.getX())
                    && Objects.equals(x.getY(), coordinate.getY()));
  }
}
