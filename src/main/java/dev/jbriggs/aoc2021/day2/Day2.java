package dev.jbriggs.aoc2021.day2;

import dev.jbriggs.aoc2021.Day;
import dev.jbriggs.aoc2021.util.PuzzleInputParser;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class Day2 extends Day<String> {

  public static final int VALUE_GROUP_NUMBER = 2;
  private static final Pattern PATTERN = Pattern.compile("^(\\w+) (\\d+)$");

  public Day2(PuzzleInputParser puzzleInputParser) {
    super(puzzleInputParser);
  }

  @Override
  public String[] getInput() {
    return getPuzzleInputParser().getPuzzleInputFromFile(inputPath());
  }

  public String partOne(String[] input) {
    int depth = 0;
    int horizontalPosition = 0;
    for (String current : input) {
      Matcher matcher = PATTERN.matcher(current);
      if (matcher.matches()) {
        int result = Integer.parseInt(matcher.group(VALUE_GROUP_NUMBER));
        switch (matcher.group(1)) {
          case "forward":
            horizontalPosition += result;
            break;
          case "up":
            depth -= result;
            break;
          case "down":
            depth += result;
            break;
          default:
            return "0";
        }
      }
    }
    return String.valueOf(depth * horizontalPosition);
  }

  public String partTwo(String[] input) {

    int depth = 0;
    int horizontalPosition = 0;
    int aim = 0;
    for (String current : input) {
      Matcher matcher = PATTERN.matcher(current);
      if (matcher.matches()) {
        int result = Integer.parseInt(matcher.group(VALUE_GROUP_NUMBER));
        switch (matcher.group(1)) {
          case "forward":
            horizontalPosition += result;
            depth -= aim * result;
            break;
          case "up":
            aim += result;
            break;
          case "down":
            aim -= result;
            break;
          default:
            return "0";
        }
      }
    }
    return String.valueOf(depth * horizontalPosition);
  }

  @Override
  public Integer day() {
    return 2;
  }

  @Override
  public String inputPath() {
    return "inputdata/day2/input.txt";
  }

  @Override
  public Type getInputType() {
    return Type.STRING;
  }
}
